#include <smunix/meat/driver.hh>
#include <smunix/core-module.hh>

namespace s = smunix;

struct TypeVis {
  using Return = void;
  template<class T> Return visit(T&& t) {
    std::ostringstream os1;
    os1.width(20);
    os1.fill(' ');
    os1 << "<" << s::cpp::demangle<T>() << ">";

    std::ostringstream os2;
    os2.width(20);
    os2.fill(' ');
    os2 << "id=" << t.id();
    std::cout.width(41);
    std::cout.fill(' ');
    std::cout << std::left << os1.str() << os2.str() << std::endl;
  }
  template<class T> Return begin(T&& t) {
  }
  template<class T> Return end(T&& t) {
  }
};

struct Vis {
  using Return = void;
  template<class T> Return visit(T&& t) {
  }
  template<class T> Return begin(T&& t) {
  }
  template<class T> Return end(T&& t) {
  }
};

struct CppVis : public Vis {
#define cpp_log(x) do { std::cout << indentStr() << x << std::endl; } while(0)
  using Return = Vis::Return;
  template<class T> Return visit(T&& t) {
  }
  template<class T> Return begin(T&& t) {
  }
  template<class T> Return end(T&& t) {
  }
  // meat::Import
  Return visit(s::r<meat::Import> aimport) {
    auto str = aimport.filename;
    boost::replace_all(str, ".meat", ".h");
    cpp_log("#include " << str);
  }
  // meat::File
  Return visit(s::r<meat::File> aFile) {
    cpp_log("#include <type_traits>");    
    cpp_log("#include <smunix/core-module.hh>");    
  }
  
  Return begin(s::r<meat::File> aFile) {
    using std::chrono::system_clock;
    auto replace = [](s::r<std::string const> src_ext, s::r<std::string const> dst_ext, s::r<std::string const> txt) -> std::string
      {
        auto str = txt;
        boost::replace_all(str, src_ext, dst_ext);
        return str;
      };
    cpp_log("");
    cpp_log("#pragma once");
    cpp_log("");
    cpp_log("/*");
    cpp_log("File Generated from : " << aFile.id());
#ifndef __GNUG__
    std::time_t tt = system_clock::to_time_t (system_clock::now());
    struct std::tm * ptm = std::localtime(&tt);
    cpp_log("Time Generated : " << std::put_time(ptm, "%c"));
#endif    
    cpp_log("Author : Providence Salumu <Providence.Salumu@smunix.com>");
    cpp_log("File Generated from : " << aFile.id());
    cpp_log("using \"build/scratch/grill/grill " << aFile.id() << " &> " << replace(".meat", ".h", aFile.id()) << "\"");
    cpp_log("Copyright 2015 SMUNIX Inc.");
    cpp_log("*/");
    cpp_log("");
  }
  Return end(s::r<meat::File> aFile) {
  }  
  // meat::Module
  Return visit(s::r<meat::Module> aModule) {
  }
  Return begin(s::r<meat::Module> aModule) {
    cpp_log("");
    cpp_log("namespace " << aModule.id() << " {");
    indent();
    cpp_log("namespace s = smunix;");    
  }
  Return end(s::r<meat::Module> aModule) {
    unindent();
    cpp_log("} // namespace " << aModule.id());
  }
  // meat::Field
  struct IShow {
    using sp = s::sp<IShow>;
    virtual ~IShow() {}
    virtual std::string show() = 0;
  };
  struct Field : IShow {
    using sp = s::sp<Field>;
    using vec = std::vector<sp>;
    
    struct IType : IShow {
      using sp = s::sp<IType>;
      virtual ~IType() {}
      virtual void inner(sp) = 0;
      std::string show() override {
        return (mInner ? mInner->show() : "itype");
      }
    private:
      sp mInner;
    };
    struct IQual : IShow {
      using sp = s::sp<IQual>;
      enum class Selector { Tag, Grp, };
      IQual(Selector slr, uint16_t n) : selector(slr), n(n) {}
      virtual ~IQual() {}
      virtual uint16_t tag() const {
        return n;
      }
      virtual std::string showIQualSet() const = 0;
      virtual std::string showIQualGet() const = 0;
      Selector type() const {
        return selector;
      }
      Selector selector;
      uint16_t n;
    };
    struct TagQType : Field::IQual {
      static constexpr IQual::Selector ctype() { return IQual::Selector::Tag; }
      TagQType(uint16_t n, s::r<std::string const> tp) : Field::IQual(IQual::Selector::Tag, n), tp(tp) {}
      std::string showIQualSet() const override {
        return "SetTag";
      }
      std::string showIQualGet() const override {
        return "GetTag";
      }
      std::string show() override {
        return std::to_string(tag());
      }
      std::string tp;
    };
    struct GrpQType : Field::IQual {
      static constexpr IQual::Selector ctype() { return IQual::Selector::Grp; }
      GrpQType(uint16_t n) : Field::IQual(IQual::Selector::Grp, n) {}
      std::string showIQualSet() const override {
        return "SetGrp";
      }
      std::string showIQualGet() const override {
        return "GetGrp";
      }
      std::string show() override {
        return std::to_string(tag());
      }
    };
    template<class T, class U>
    static s::sp<T> castQual(s::sp<U> u) {
      if (u->type() == T::ctype()) {
        return s::asp<T>(u);
      }
      return s::sp<T>(nullptr);
    }

    Field(s::r<std::string const> n) : n(n) {}
    std::string show() override {
      std::ostringstream os;
      os << "s::data<" << (p ? p->show() : "untype") << ">" << " _" << n;
      return  os.str();
    }
    void inner(IType::sp ap) {
      if(not p)
        p = ap;
      else
        p->inner(ap);
    }
    void qualifier(IQual::sp aq) {
      q = aq;
    }
    std::string n;
    IType::sp p;
    IQual::sp q;
  };
  Return visit(s::r<meat::Field> aField) {
  }
  Return begin(s::r<meat::Field> aField) {
    mRecFields.back().emplace_back(std::make_shared<Field>(aField.id()));
  }
  Return end(s::r<meat::Field> aField) {
    cpp_log(mRecFields.back().back()->show() << ";");
  }
  // meat::Record
  Return visit(s::r<meat::Record> aRecord) {
  }
  Return begin(s::r<meat::Record> aRecord) {
    mRecFields.emplace_back(); // Field::vec
    cpp_log("struct " << aRecord.id() << " {");
    indent();
  }
  Return end(s::r<meat::Record> aRecord) {
    // tag switch gen
    auto &lFields = mRecFields.back();
    // describe fields
    cpp_log("");
    cpp_log("// all accessors - get");
    for (auto &f : lFields) {
      cpp_log("" << f->p->show() << "& " << f->n << "() { " << "return *_" << f->n << ";" << " }");
      cpp_log("" << f->p->show() << " const& " << f->n << "() const { " << "return *_" << f->n << ";" << " }");
    }
    
    cpp_log("");
    cpp_log("// all fields");
    cpp_log("template<class T> static void describe(T&& t) {");
    indent();
    cpp_log(aRecord.id() << " l" << aRecord.id() << ";");
    cpp_log("using Describe = s::fields::Describe<typename std::decay<T>::type>;");
    for (auto &f : lFields) 
      cpp_log("Describe::template apply<" << f->p->show() << ">(\"" << f->n << "\", s::p<uint8_t>(s::addr(" << "l" << aRecord.id() << "." << f->n << "())) - s::p<uint8_t>(s::addr(" << "l" << aRecord.id() << ")), std::forward<T>(t));");
    unindent();
    cpp_log("}");
    
    if (std::any_of(lFields.begin(), lFields.end(), [](s::r<Field::sp const> f) -> bool { return not not f->q; })) {
      cpp_log("");
      cpp_log("// setter");
      cpp_log("template<class T> void set(bool& success, uint16_t n, T&& t) {");
      indent();
      cpp_log("using SetTag = s::tag::Set<typename std::decay<T>::type>;");
      cpp_log("using SetGrp = s::grp::Set<typename std::decay<T>::type>;");
      cpp_log("switch(n) {");
      indent();
      for (auto &f : lFields) 
        if (f->q) {
          cpp_log("case " << f->q->tag() << ": " << f->q->showIQualSet() << "::template apply(success, " << f->n << "(), std::forward<T>(t)); return;");
        }
      cpp_log("default: success = false; return;");
      unindent();
      cpp_log("}");
      unindent();
      cpp_log("}");

      cpp_log("");
      cpp_log("// getter");
      cpp_log("template<class T> void get(bool& success, T&& t) {");
      indent();
      cpp_log("using GetTag = s::tag::Get<typename std::decay<T>::type>;");
      cpp_log("using GetGrp = s::grp::Get<typename std::decay<T>::type>;");
      for (auto &f : lFields) 
        if (f->q) {
          cpp_log(f->q->showIQualGet() << "::template apply(success, " << f->n << "(), " << f->q->tag() << ", std::forward<T>(t));");
        }
      unindent();
      cpp_log("}");
    }
    unindent();
    cpp_log("};");
    mRecFields.pop_back();
  }
  // meat::ReferencedTypePrefix
  Return visit(s::r<meat::ReferencedTypePrefix> aReferencedTypePrefix) {
    struct RefType : Field::IType {
      void inner(Field::IType::sp ap) override {
        if (not p)
          p = ap;
        else
          p->inner(ap);
      }
      std::string show() override {
        std::ostringstream os;
        os << "s::off::Ref<" << (p ? p->show() : "untype") << ">";
        return os.str();
      }
      Field::IType::sp p;
    };
    
    mRecFields.back().back()->inner(std::make_shared<RefType>());
  }
  Return begin(s::r<meat::ReferencedTypePrefix> aReferencedTypePrefix) {
  }
  Return end(s::r<meat::ReferencedTypePrefix> aReferencedTypePrefix) {
  }
  // meat::OptionalTypePrefix
  Return visit(s::r<meat::OptionalTypePrefix> aOptionalTypePrefix) {
    struct OptType : Field::IType {
      void inner(Field::IType::sp ap) override {
        if (not p)
          p = ap;
        else
          p->inner(ap);
      }
      std::string show() override {
        std::ostringstream os;
        os << "s::maybe<" << (p ? p->show() : "untype") << ">";
        return os.str();
      }
      Field::IType::sp p;
    };
    mRecFields.back().back()->inner(std::make_shared<OptType>());
  }
  Return begin(s::r<meat::OptionalTypePrefix> aOptionalTypePrefix) {
  }
  Return end(s::r<meat::OptionalTypePrefix> aOptionalTypePrefix) {
  }
  // meat::DynArrTypePrefix
  Return visit(s::r<meat::DynArrTypePrefix> aDynArrTypePrefix) {
    struct DynArrType : Field::IType {
      void inner(Field::IType::sp ap) override {
        if (not p)
          p = ap;
        else
          p->inner(ap);
      }
      std::string show() override {
        std::ostringstream os;
        os << "s::dyn::arr<" << (p ? p->show() : "untype") << ">";
        return os.str();
      }
      Field::IType::sp p;
    };
    mRecFields.back().back()->inner(std::make_shared<DynArrType>());
  }
  Return begin(s::r<meat::DynArrTypePrefix> aDynArrTypePrefix) {
  }
  Return end(s::r<meat::DynArrTypePrefix> aDynArrTypePrefix) {
  }
  // meat::FixArrTypePrefix
  Return visit(s::r<meat::FixArrTypePrefix> aFixArrTypePrefix) {
    struct FixArrType : Field::IType {
      FixArrType(size_t len) : len(len) {}
      void inner(Field::IType::sp ap) override {
        if (not p)
          p = ap;
        else
          p->inner(ap);
      }
      std::string show() override {
        std::ostringstream os;
        os << "s::fix::arr<" << (p ? p->show() : "untyped") << ", " << len << ">";
        return os.str();
      }
      size_t len = 0;
      Field::IType::sp p;
    };
    mRecFields.back().back()->inner(std::make_shared<FixArrType>(aFixArrTypePrefix.len));
  }
  Return begin(s::r<meat::FixArrTypePrefix> aFixArrTypePrefix) {
  }
  Return end(s::r<meat::FixArrTypePrefix> aFixArrTypePrefix) {
  }
  // meat::ScalarType
  Return visit(s::r<meat::ScalarType> aScalarType) {
    struct ScalType : Field::IType {
      ScalType(s::r<std::string const> n) : n(n) {}
      void inner(Field::IType::sp ap) override {
      }
      std::string show() override {
        std::string str = n;
        boost::replace_all(str, ".", "::");
        return str;
      }
      std::string n;
    };
    mRecFields.back().back()->inner(std::make_shared<ScalType>(aScalarType.id()));
  }
  Return begin(s::r<meat::ScalarType> aScalarType) {
  }
  Return end(s::r<meat::ScalarType> aScalarType) {
  }
  // meat::TypeSfxQualTag
  Return visit(s::r<meat::TypeSfxQualTag> aTypeSfxQualTag) {
    mRecFields.back().back()->qualifier(std::make_shared<Field::TagQType>(aTypeSfxQualTag.tag, aTypeSfxQualTag.type->id()));
  }
  Return begin(s::r<meat::TypeSfxQualTag> aTypeSfxQualTag) {
  }
  Return end(s::r<meat::TypeSfxQualTag> aTypeSfxQualTag) {
  }
  // meat::TypeSfxQualGrp
  Return visit(s::r<meat::TypeSfxQualGrp> aTypeSfxQualGrp) {
    mRecFields.back().back()->qualifier(std::make_shared<Field::GrpQType>(aTypeSfxQualGrp.tag));
  }
  Return begin(s::r<meat::TypeSfxQualGrp> aTypeSfxQualGrp) {
  }
  Return end(s::r<meat::TypeSfxQualGrp> aTypeSfxQualGrp) {
  }
  // meat::Enum
  Return visit(s::r<meat::Enum> aEnum) {
  }
  Return begin(s::r<meat::Enum> aEnum) {
    cpp_log("enum class " << aEnum.id() << " {");
    indent();
  }
  Return end(s::r<meat::Enum> aEnum) {
    unindent();
    cpp_log("};");
  }
  // meat::Enum::Entry
  Return visit(s::r<meat::Enum::Entry> aEnum) {
    cpp_log(aEnum.id() << " = " << std::to_string(aEnum.value) << ",");
  }
  Return begin(s::r<meat::Enum::Entry> aEnum) {
  }
  Return end(s::r<meat::Enum::Entry> aEnum) {
  }
  
private:
  void indent() {
    mIndentation += 2;
  }
  void unindent() {
    mIndentation -= 2;
    if (indentation() < 0)
      mIndentation = 0;
  }
  int8_t indentation() const {
    return mIndentation;
  }
  std::string indentStr() const {
    return std::string(indentation(), ' ');
  }
  int8_t mIndentation = 0;
  std::vector<Field::vec> mRecFields;
};

template<class T> using arr = T[];
int main(int argc, arr<char const*> argv) {
  int res = 0;
  meat::driver drv;
  for (int i=1; i<argc; ++i)
    if(argv[i] == std::string("-p"))
      drv.trace_parse(true);
    else if (argv[i] == std::string("-s"))
      drv.trace_scan(true);
    else if (not drv.parse(argv[i]))
      res = -1;
    else
      res = 0;
#if 0  
  TypeVis vis;
#else
  CppVis vis;
#endif  
  drv.visit(vis);
  return res;
}
