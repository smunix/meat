
#pragma once

/*
File Generated from : samples/meat/m1.meat
Author : Providence Salumu <Providence.Salumu@smunix.com>
File Generated from : samples/meat/m1.meat
using "build/scratch/grill/grill samples/meat/m1.meat &> samples/meat/m1.h"
Copyright 2015 SMUNIX Inc.
*/

#include <type_traits>
#include <smunix/core-module.hh>

namespace m1 {
  namespace s = smunix;
  enum class Fruits {
    Apple = 1,
    Potatoe = 2,
  };
  enum class Animals {
    Lion = 1,
    Chimp = 2,
  };
  struct Simple1 {
    s::data<char> _f1;
    
    // all accessors - get
    char& f1() { return *_f1; }
    char const& f1() const { return *_f1; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple1 lSimple1;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple1.f1())) - s::p<uint8_t>(s::addr(lSimple1)), std::forward<T>(t));
    }
  };
  struct Simple2 {
    s::data<char> _f1;
    s::data<uint8_t> _f2;
    
    // all accessors - get
    char& f1() { return *_f1; }
    char const& f1() const { return *_f1; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple2 lSimple2;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple2.f1())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple2.f2())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
    }
  };
  struct Simple3 {
    s::data<char> _f1;
    s::data<uint8_t> _f2;
    s::data<s::fix::arr<char, 10>> _f3f;
    s::data<s::off::Ref<s::dyn::arr<char>>> _f4d;
    s::data<s::maybe<char>> _f5o;
    s::data<s::maybe<Simple2>> _f6o;
    
    // all accessors - get
    char& f1() { return *_f1; }
    char const& f1() const { return *_f1; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    s::fix::arr<char, 10>& f3f() { return *_f3f; }
    s::fix::arr<char, 10> const& f3f() const { return *_f3f; }
    s::off::Ref<s::dyn::arr<char>>& f4d() { return *_f4d; }
    s::off::Ref<s::dyn::arr<char>> const& f4d() const { return *_f4d; }
    s::maybe<char>& f5o() { return *_f5o; }
    s::maybe<char> const& f5o() const { return *_f5o; }
    s::maybe<Simple2>& f6o() { return *_f6o; }
    s::maybe<Simple2> const& f6o() const { return *_f6o; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple3 lSimple3;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple3.f1())) - s::p<uint8_t>(s::addr(lSimple3)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple3.f2())) - s::p<uint8_t>(s::addr(lSimple3)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<char, 10>>("f3f", s::p<uint8_t>(s::addr(lSimple3.f3f())) - s::p<uint8_t>(s::addr(lSimple3)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("f4d", s::p<uint8_t>(s::addr(lSimple3.f4d())) - s::p<uint8_t>(s::addr(lSimple3)), std::forward<T>(t));
      Describe::template apply<s::maybe<char>>("f5o", s::p<uint8_t>(s::addr(lSimple3.f5o())) - s::p<uint8_t>(s::addr(lSimple3)), std::forward<T>(t));
      Describe::template apply<s::maybe<Simple2>>("f6o", s::p<uint8_t>(s::addr(lSimple3.f6o())) - s::p<uint8_t>(s::addr(lSimple3)), std::forward<T>(t));
    }
  };
  struct Simple4 {
    s::data<char> _f1;
    s::data<uint8_t> _f2;
    s::data<s::fix::arr<char, 10>> _f3f;
    s::data<s::off::Ref<s::dyn::arr<char>>> _f4d;
    s::data<Simple3> _f5;
    s::data<s::off::Ref<s::dyn::arr<Simple3>>> _f6d;
    s::data<s::fix::arr<Simple3, 20>> _f5f;
    
    // all accessors - get
    char& f1() { return *_f1; }
    char const& f1() const { return *_f1; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    s::fix::arr<char, 10>& f3f() { return *_f3f; }
    s::fix::arr<char, 10> const& f3f() const { return *_f3f; }
    s::off::Ref<s::dyn::arr<char>>& f4d() { return *_f4d; }
    s::off::Ref<s::dyn::arr<char>> const& f4d() const { return *_f4d; }
    Simple3& f5() { return *_f5; }
    Simple3 const& f5() const { return *_f5; }
    s::off::Ref<s::dyn::arr<Simple3>>& f6d() { return *_f6d; }
    s::off::Ref<s::dyn::arr<Simple3>> const& f6d() const { return *_f6d; }
    s::fix::arr<Simple3, 20>& f5f() { return *_f5f; }
    s::fix::arr<Simple3, 20> const& f5f() const { return *_f5f; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple4 lSimple4;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple4.f1())) - s::p<uint8_t>(s::addr(lSimple4)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple4.f2())) - s::p<uint8_t>(s::addr(lSimple4)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<char, 10>>("f3f", s::p<uint8_t>(s::addr(lSimple4.f3f())) - s::p<uint8_t>(s::addr(lSimple4)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("f4d", s::p<uint8_t>(s::addr(lSimple4.f4d())) - s::p<uint8_t>(s::addr(lSimple4)), std::forward<T>(t));
      Describe::template apply<Simple3>("f5", s::p<uint8_t>(s::addr(lSimple4.f5())) - s::p<uint8_t>(s::addr(lSimple4)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<Simple3>>>("f6d", s::p<uint8_t>(s::addr(lSimple4.f6d())) - s::p<uint8_t>(s::addr(lSimple4)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<Simple3, 20>>("f5f", s::p<uint8_t>(s::addr(lSimple4.f5f())) - s::p<uint8_t>(s::addr(lSimple4)), std::forward<T>(t));
    }
  };
  struct Simple5 {
    s::data<char> _f1;
    s::data<uint8_t> _f2;
    s::data<s::fix::arr<char, 10>> _f3;
    s::data<s::off::Ref<s::dyn::arr<char>>> _f4;
    s::data<Simple3> _f5;
    s::data<s::off::Ref<s::dyn::arr<Simple3>>> _f5d;
    s::data<s::fix::arr<Simple3, 20>> _f5f;
    s::data<s::off::Ref<Simple4>> _f6r;
    s::data<s::off::Ref<s::fix::arr<Simple4, 20>>> _f7rf;
    s::data<s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>>> _f8od;
    s::data<s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>>> _f9ord;
    
    // all accessors - get
    char& f1() { return *_f1; }
    char const& f1() const { return *_f1; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    s::fix::arr<char, 10>& f3() { return *_f3; }
    s::fix::arr<char, 10> const& f3() const { return *_f3; }
    s::off::Ref<s::dyn::arr<char>>& f4() { return *_f4; }
    s::off::Ref<s::dyn::arr<char>> const& f4() const { return *_f4; }
    Simple3& f5() { return *_f5; }
    Simple3 const& f5() const { return *_f5; }
    s::off::Ref<s::dyn::arr<Simple3>>& f5d() { return *_f5d; }
    s::off::Ref<s::dyn::arr<Simple3>> const& f5d() const { return *_f5d; }
    s::fix::arr<Simple3, 20>& f5f() { return *_f5f; }
    s::fix::arr<Simple3, 20> const& f5f() const { return *_f5f; }
    s::off::Ref<Simple4>& f6r() { return *_f6r; }
    s::off::Ref<Simple4> const& f6r() const { return *_f6r; }
    s::off::Ref<s::fix::arr<Simple4, 20>>& f7rf() { return *_f7rf; }
    s::off::Ref<s::fix::arr<Simple4, 20>> const& f7rf() const { return *_f7rf; }
    s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>>& f8od() { return *_f8od; }
    s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>> const& f8od() const { return *_f8od; }
    s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>>& f9ord() { return *_f9ord; }
    s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>> const& f9ord() const { return *_f9ord; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple5 lSimple5;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple5.f1())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple5.f2())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<char, 10>>("f3", s::p<uint8_t>(s::addr(lSimple5.f3())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("f4", s::p<uint8_t>(s::addr(lSimple5.f4())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<Simple3>("f5", s::p<uint8_t>(s::addr(lSimple5.f5())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<Simple3>>>("f5d", s::p<uint8_t>(s::addr(lSimple5.f5d())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<Simple3, 20>>("f5f", s::p<uint8_t>(s::addr(lSimple5.f5f())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<Simple4>>("f6r", s::p<uint8_t>(s::addr(lSimple5.f6r())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::fix::arr<Simple4, 20>>>("f7rf", s::p<uint8_t>(s::addr(lSimple5.f7rf())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>>>("f8od", s::p<uint8_t>(s::addr(lSimple5.f8od())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
      Describe::template apply<s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>>>("f9ord", s::p<uint8_t>(s::addr(lSimple5.f9ord())) - s::p<uint8_t>(s::addr(lSimple5)), std::forward<T>(t));
    }
  };
  struct Simple6 {
    s::data<char> _f1t;
    s::data<uint8_t> _f2;
    s::data<s::fix::arr<char, 10>> _f3ft;
    s::data<s::off::Ref<s::dyn::arr<char>>> _f4dt;
    s::data<Simple3> _f5;
    s::data<s::off::Ref<s::dyn::arr<Simple3>>> _f5dg;
    s::data<s::fix::arr<Simple3, 20>> _f5f;
    s::data<s::off::Ref<Simple4>> _f6r;
    s::data<s::off::Ref<s::fix::arr<Simple4, 20>>> _f7rf;
    s::data<s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>>> _f8rd;
    s::data<s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>>> _f9ord;
    
    // all accessors - get
    char& f1t() { return *_f1t; }
    char const& f1t() const { return *_f1t; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    s::fix::arr<char, 10>& f3ft() { return *_f3ft; }
    s::fix::arr<char, 10> const& f3ft() const { return *_f3ft; }
    s::off::Ref<s::dyn::arr<char>>& f4dt() { return *_f4dt; }
    s::off::Ref<s::dyn::arr<char>> const& f4dt() const { return *_f4dt; }
    Simple3& f5() { return *_f5; }
    Simple3 const& f5() const { return *_f5; }
    s::off::Ref<s::dyn::arr<Simple3>>& f5dg() { return *_f5dg; }
    s::off::Ref<s::dyn::arr<Simple3>> const& f5dg() const { return *_f5dg; }
    s::fix::arr<Simple3, 20>& f5f() { return *_f5f; }
    s::fix::arr<Simple3, 20> const& f5f() const { return *_f5f; }
    s::off::Ref<Simple4>& f6r() { return *_f6r; }
    s::off::Ref<Simple4> const& f6r() const { return *_f6r; }
    s::off::Ref<s::fix::arr<Simple4, 20>>& f7rf() { return *_f7rf; }
    s::off::Ref<s::fix::arr<Simple4, 20>> const& f7rf() const { return *_f7rf; }
    s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>>& f8rd() { return *_f8rd; }
    s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>> const& f8rd() const { return *_f8rd; }
    s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>>& f9ord() { return *_f9ord; }
    s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>> const& f9ord() const { return *_f9ord; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple6 lSimple6;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1t", s::p<uint8_t>(s::addr(lSimple6.f1t())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple6.f2())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<char, 10>>("f3ft", s::p<uint8_t>(s::addr(lSimple6.f3ft())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("f4dt", s::p<uint8_t>(s::addr(lSimple6.f4dt())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<Simple3>("f5", s::p<uint8_t>(s::addr(lSimple6.f5())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<Simple3>>>("f5dg", s::p<uint8_t>(s::addr(lSimple6.f5dg())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<Simple3, 20>>("f5f", s::p<uint8_t>(s::addr(lSimple6.f5f())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<Simple4>>("f6r", s::p<uint8_t>(s::addr(lSimple6.f6r())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::fix::arr<Simple4, 20>>>("f7rf", s::p<uint8_t>(s::addr(lSimple6.f7rf())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::maybe<s::off::Ref<s::dyn::arr<Simple4>>>>>("f8rd", s::p<uint8_t>(s::addr(lSimple6.f8rd())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
      Describe::template apply<s::maybe<s::off::Ref<s::off::Ref<s::dyn::arr<Simple4>>>>>("f9ord", s::p<uint8_t>(s::addr(lSimple6.f9ord())) - s::p<uint8_t>(s::addr(lSimple6)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 150: SetTag::template apply(success, f1t(), std::forward<T>(t)); return;
        case 58: SetTag::template apply(success, f3ft(), std::forward<T>(t)); return;
        case 59: SetTag::template apply(success, f4dt(), std::forward<T>(t)); return;
        case 600: SetGrp::template apply(success, f5dg(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, f1t(), 150, std::forward<T>(t));
      GetTag::template apply(success, f3ft(), 58, std::forward<T>(t));
      GetTag::template apply(success, f4dt(), 59, std::forward<T>(t));
      GetGrp::template apply(success, f5dg(), 600, std::forward<T>(t));
    }
  };
  struct Extend1 {
    s::data<Simple1> _s1;
    s::data<Simple2> _s2;
    s::data<uint8_t> _f2;
    
    // all accessors - get
    Simple1& s1() { return *_s1; }
    Simple1 const& s1() const { return *_s1; }
    Simple2& s2() { return *_s2; }
    Simple2 const& s2() const { return *_s2; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Extend1 lExtend1;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<Simple1>("s1", s::p<uint8_t>(s::addr(lExtend1.s1())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
      Describe::template apply<Simple2>("s2", s::p<uint8_t>(s::addr(lExtend1.s2())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lExtend1.f2())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
    }
  };
  struct Extend2 {
    s::data<Simple1> _s1;
    s::data<Simple2> _s2;
    s::data<uint8_t> _f2;
    
    // all accessors - get
    Simple1& s1() { return *_s1; }
    Simple1 const& s1() const { return *_s1; }
    Simple2& s2() { return *_s2; }
    Simple2 const& s2() const { return *_s2; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Extend2 lExtend2;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<Simple1>("s1", s::p<uint8_t>(s::addr(lExtend2.s1())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
      Describe::template apply<Simple2>("s2", s::p<uint8_t>(s::addr(lExtend2.s2())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lExtend2.f2())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
    }
  };
} // namespace m1
