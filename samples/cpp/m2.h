
#pragma once

/*
File Generated from : samples/meat/m2.meat
Author : Providence Salumu <Providence.Salumu@smunix.com>
File Generated from : samples/meat/m2.meat
using "build/scratch/grill/grill samples/meat/m2.meat &> samples/meat/m2.h"
Copyright 2015 SMUNIX Inc.
*/

#include <type_traits>
#include <smunix/core-module.hh>
#include "m1.h"

namespace m2 {
  namespace s = smunix;
  struct Simple1 {
    s::data<char> _f1;
    
    // all accessors - get
    char& f1() { return *_f1; }
    char const& f1() const { return *_f1; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple1 lSimple1;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple1.f1())) - s::p<uint8_t>(s::addr(lSimple1)), std::forward<T>(t));
    }
  };
  struct Simple2 {
    s::data<char> _f1;
    s::data<uint8_t> _f2;
    
    // all accessors - get
    char& f1() { return *_f1; }
    char const& f1() const { return *_f1; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Simple2 lSimple2;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple2.f1())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple2.f2())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
    }
  };
  struct Extend1 {
    s::data<Simple1> _s1;
    s::data<m1::Simple2> _s2;
    s::data<uint8_t> _f2;
    
    // all accessors - get
    Simple1& s1() { return *_s1; }
    Simple1 const& s1() const { return *_s1; }
    m1::Simple2& s2() { return *_s2; }
    m1::Simple2 const& s2() const { return *_s2; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Extend1 lExtend1;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<Simple1>("s1", s::p<uint8_t>(s::addr(lExtend1.s1())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
      Describe::template apply<m1::Simple2>("s2", s::p<uint8_t>(s::addr(lExtend1.s2())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lExtend1.f2())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
    }
  };
  struct Extend2 {
    s::data<m1::Simple1> _s1;
    s::data<Simple2> _s2;
    s::data<uint8_t> _f2;
    
    // all accessors - get
    m1::Simple1& s1() { return *_s1; }
    m1::Simple1 const& s1() const { return *_s1; }
    Simple2& s2() { return *_s2; }
    Simple2 const& s2() const { return *_s2; }
    uint8_t& f2() { return *_f2; }
    uint8_t const& f2() const { return *_f2; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Extend2 lExtend2;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<m1::Simple1>("s1", s::p<uint8_t>(s::addr(lExtend2.s1())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
      Describe::template apply<Simple2>("s2", s::p<uint8_t>(s::addr(lExtend2.s2())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
      Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lExtend2.f2())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
    }
  };
} // namespace m2
