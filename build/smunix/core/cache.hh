#pragma once
#include <stddef.h>

namespace smunix {
  enum class OS {
    APPLE,
      LINUX,
      WIN32,
  };
} // namespace smunix

//////////////////////////////////////////////////////////////////////////////////////
// source, https://github.com/NickStrupat/CacheLineSize/blob/master/CacheLineSize.c //
//////////////////////////////////////////////////////////////////////////////////////

#if defined(__APPLE__)
#include <sys/sysctl.h>
namespace smunix {
  template<OS os = OS::APPLE> struct Cache;
  
  template<> struct Cache<OS::APPLE> {
    static size_t lineSize() {
      size_t line_size = 0;
      size_t sizeof_line_size = sizeof(line_size);
      sysctlbyname("hw.cachelinesize", &line_size, &sizeof_line_size, 0, 0);
      return line_size;
    }
  };
} // namespace smunix
#endif
