#pragma once
#include <boost/variant.hpp>

namespace smunix {
  template<class ...A> using Add = boost::variant<A...>;
  template<class A> using maybe = Add<boost::blank, A>;
  template<class A, class B> using either = Add<A, B>;
} // namespace smunix
