#pragma once
#include <memory>
#include <string>
#include <functional>
#include <algorithm>
#include <chrono>
#include <cxxabi.h>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/accumulators/statistics/error_of.hpp>
#include <boost/accumulators/statistics/error_of_mean.hpp>
#include <boost/accumulators/statistics/weighted_density.hpp>
#include <boost/numeric/conversion/cast.hpp>

namespace smunix {
  namespace cpp {
    template<class T> std::string demangle() {
      int status = -4;
      std::unique_ptr<char, void(*)(void*)> p {
	abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status),
	  std::free
	  };
      return not status ? p.get() : typeid(T).name(); 
    }
  } // namespace cpp

  namespace stream {
    template<class S> struct Out {
      r<Out> operator|(r<std::string const> v) { os << v; return *this; }
      template<class T> r<Out> operator|(T&& v) { os << v; return *this; }
      r<S> operator*() { return os; }
      r<S const> operator*() const { return os; }
      S os;
    };
    template<class S> struct In {
      void operator|(r<std::string const> v) {}
    };
  } // namespace stream
  
  namespace str {
    template<class T> std::string to(T&& t) {
      return std::to_string(std::forward<T>(t));
    }

    std::string to(r<std::string const> t) {
      return t;
    }
  
    template<class T> struct Conv {
      static std::string apply(r<T> t) {
	return cpp::demangle<T>();
      }
    };
  
    template<class T> std::string conv(r<T> t) {
      return Conv<T>::apply(t);
    }
  } // namespace str
  
  namespace fields {
    template<class T> struct Describe {
      template<class V> static void apply(r<std::string const> name, size_t offset, r<T> t) {
	t.template apply<V>(name, offset);
      }
    };
  } // namespace fields

  namespace tag {
    template<class T> struct Set {
      template<class U, class V> static void apply(r<bool> rc, r<U> fld, r<V> t) {
      }
    };
    template<class T> struct Get {
      template<class U, class V> static void apply(r<bool> rc, r<U> fld, uint16_t n, r<V> t) {
	t.template applyTag<U>(n, fld);
      }
    };
    namespace fix {
      template<class T> struct Conv {
	template<class S, class Caller> static void apply(r<stream::Out<S>> os, uint16_t n, r<T> af, Caller&& c) {
	  os | str::to(n) | "=" | str::Conv<T>::apply(af) | ";";
	}
      };
      template<class T> struct Conv<off::Ref<T>> {
	template<class S, class Caller> static void apply(r<stream::Out<S>> os, uint16_t n, r<off::Ref<T>> af, Caller&& c) {
	  if (af)
	    Conv<T>::apply(os, n, *af, std::forward<Caller>(c));
	}
      };
      template<class T, class C> struct Conv<dyn::arr<T, C>> {
	template<class S, class Caller> static void apply(r<stream::Out<S>> os, uint16_t n, r<dyn::arr<T, C>> af, Caller&& c) {
	  os | str::to(n) | "=";
	  for (auto& e: af)
	    os | str::Conv<T>::apply(e);
	  os | ";";
	}
      };
      template<class C> struct Conv<dyn::arr<char, C>> {
	template<class S, class Caller> static void apply(r<stream::Out<S>> os, uint16_t n, r<dyn::arr<char, C>> af, Caller&& c) {
	  os | str::to(n) | "=" | std::string(af.begin(), af.len) | ";";
	}
      };
    } // namespace fix
  } // namespace tag

  namespace grp {
    template<class T> struct Set {
      template<class U, class V> static void apply(r<bool> rc, r<U> fld, r<V> t) {
      }
    };
    template<class T> struct Get {
      template<class U, class V> static void apply(r<bool> rc, r<U> fld, uint16_t n, r<V> t) {
	t.template applyGrp<U>(n, fld);
      }
    };
    namespace fix {
      template<class T> struct Conv {
	template<class S, class Caller> static void apply(r<stream::Out<S>> os, uint16_t n, r<T> af, Caller&& c) {
	  if (af.len) {
	    os | str::to(n) | "=" | str::to(af.len) | ";";
	    bool success = false;
	    for (auto& e: af)
	      e.get(success, std::forward<Caller>(c));
	  }
	}
      };
      template<class T> struct Conv<off::Ref<T>> {
	template<class S, class Caller> static void apply(r<stream::Out<S>> os, uint16_t n, r<off::Ref<T>> af, Caller&& c) {
	  if (af)
	    Conv<T>::apply(os, n, *af, std::forward<Caller>(c));
	}
      };
    } // namespace fix
  } // namespace grp

  namespace stats {
    template<class T> using vec_t = std::vector<T>;
    namespace bac = boost::accumulators;

    enum class Report {
      Simple,
	NoMax,
	NoMaxPercent,
    };

    template<class Ev, size_t Sz = 1000000> struct Base {
      using event_t = Ev;
      using interval_t = typename event_t::interval_t;
      using events_t = var::arr<event_t, Sz + 2>;
      using intervals_t = vec_t<interval_t>;
      static constexpr size_t MaxSize = Sz;

      using accumulators_t = bac::accumulator_set<interval_t,
						  bac::features<
						    bac::tag::count,
						    bac::tag::min,
						    bac::tag::max,
						    bac::tag::mean,
						    bac::tag::variance>>;
      using mean_errors_t = bac::accumulator_set<interval_t,
						 bac::stats<
						   bac::tag::error_of<bac::tag::mean>>>;
      using density_t = bac::accumulator_set<interval_t,
					     bac::stats<
					       bac::tag::density>>;
    public:
      Base() { reset(); }
      void reset() {
	events.reset();
      }
      template<class Fn> void start(Fn&& fn) {
	fn(ev);
      }
      template<class Fn> void stop(Fn&& fn) {
	fn(ev);
	events.push_back(ev);
      }
      template<class S, class FnS, class FnE> void warmup(r<stream::Out<S>> os, FnS&& fns, FnE&& fne, Report rpt, size_t sz = 1000, bool display = false) {
	reset();
	size_t i = 0;
	while (i++ < sz) {
	  start(std::forward<FnS>(fns)); stop(std::forward<FnE>(fne));
	}
	show(os, "Warming up", rpt, display);
	reset();
      }
      template<class FnS, class FnE, class InvokeFn> void run(FnS&& startF, InvokeFn&& invokeF, FnE&& endF) {
	for (size_t i = 0; i < MaxSize; ++i) {
	  start(std::forward<FnS>(startF));
	  invokeF();
	  stop(std::forward<FnE>(endF));
	}
      }
      template<class S> void show(r<stream::Out<S>> os, r<std::string const> header, Report rpt, bool display = false) const {
	intervals_t intervals;
	auto rmMax = [&intervals]()->void { intervals.erase(std::max_element(intervals.begin(), intervals.end())); };
	  
	size_t nsize = 0;
	// convert from event -> interval
	intervals.reserve(events.size());
	for (auto& e: events)
	  intervals.push_back(e.asInterval());
	// adjust the interval set
	if (not intervals.empty()) {
	  switch(rpt) {
	  case Report::Simple :
	    break;
	  case Report::NoMax :
	    rmMax();
	    break;
	  case Report::NoMaxPercent :
	    nsize = intervals.size() * 9/10;
	    while(intervals.size() > nsize) rmMax() ;
	    break;
	  default : break;
	  }
	}
	// build accumulators
	accumulators_t accumulators;
	mean_errors_t merrors;
	for (auto& i: intervals) {
	  accumulators(i); merrors(i);
	  if (display) os | event_t::toString(i) | "\n";
	}
	os | header | "\n"
	  | std::string(80, '-') | "\n"
	  | "count: " | event_t::toString(bac::count(accumulators)) | "\n"
	  | "min: "   | event_t::toString(bac::min(accumulators)) | "\n"
	  | "mean: "  | event_t::toString(bac::mean(accumulators)) | "\n"
	  | "variance : "  | event_t::toString(bac::variance(accumulators)) | "\n"
	  | "max: "   | event_t::toString(bac::max(accumulators)) | "\n"
	  | "err: "   | event_t::toString(bac::error_of<bac::tag::mean>(merrors)) | "\n"
	  | std::string(80, '-') | "\n\n";
      }
      event_t	ev;
      events_t	events;
      size_t	len;
    };
  } // namespace stats
  namespace linear {
    template<template<size_t> class F, size_t Start, size_t Next, size_t End, size_t Current = Start, bool Run = (Start < Next and Next < End and Current < End)> struct Regression {
      static constexpr size_t Delta() { return Next - Start; }
      static void apply() {
	using Fn = F<Current>;
	using NextLR = Regression<F, Start + Delta(), Next + Delta(), End>;

	Fn::apply();
	NextLR::apply();
      }
    };
    template<template<size_t> class F, size_t Start, size_t Next, size_t End, size_t Current> struct Regression<F, Start, Next, End, Current, false> { static void apply() {} };
  } // namespace linear
} // namespace smunix

