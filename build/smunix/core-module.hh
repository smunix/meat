
#ifdef SMUNIX_MODULE_COMPILATION
# error "Do not include smunix module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef SMUNIX_MODULE_CORE
    

// Module headers:
#include "core/array.hh"
#include "core/buffer.hh"
#include "core/cache.hh"
#include "core/memory.hh"
#include "core/ref.hh"
#include "core/smunix.hh"
#include "core/utils.hh"
#include "core/variant.hh"
#endif
