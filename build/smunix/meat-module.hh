
#ifdef SMUNIX_MODULE_COMPILATION
# error "Do not include smunix module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef SMUNIX_MODULE_MEAT
    

// Module headers:
#include "meat/alloc.hh"
#include "meat/driver.hh"
#include "meat/location.hh"
#include "meat/parser.hh"
#include "meat/position.hh"
#include "meat/semantics.hh"
#include "meat/stack.hh"
#endif
