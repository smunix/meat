%{
#include <smunix/meat/driver.hh>
#include <smunix/meat/parser.hh>

#undef yywrap
#define yywrap() 1

#define get_string(s) std::string(yytext, yytext + yyleng - s)

static meat::yy::location loc;
int yyShouldIndent = 0;
#if 0
meat::yy::parser::symbol_type yyStart(meat::yy::parser::token::END, loc);
#endif
bool shouldIndent() {
    return yyShouldIndent > 0;
}
void shouldIndent(bool v) {
    if (v) {
	++yyShouldIndent;
    }
    else if (yyShouldIndent > 0) {
	--yyShouldIndent;
    }
}

%}

%option noyywrap nounput batch debug noinput
%x COMMENT_BLOCK
			
lower_c_letter [a-z]
upper_c_letter [A-Z]
underscore _			
digit [0-9]			
integer {digit}+
blank [ \t]
id [a-zA-Z][a-zA-Z0-9_]*
			
%{
  // code that runs each time a pattern is matched
#define YY_USER_ACTION loc.columns(yyleng);
%}			

%%			
%{
// code that runs each time yylex is called
loc.step();
%}

<<EOF>>			{ return meat::yy::parser::make_END(loc); }

"\n "			{ loc.lines(yyleng); loc.step(); if (shouldIndent()) { return meat::yy::parser::make_Indent(loc); } }
"\r "			{ loc.step(); if (shouldIndent()) { return meat::yy::parser::make_Indent(loc); } }
"\n\t "			{ loc.lines(yyleng); loc.step(); if (shouldIndent()) { return meat::yy::parser::make_Indent(loc); } }
"\r\t "			{ loc.step(); if (shouldIndent()) { return meat::yy::parser::make_Indent(loc); } }
"\n\t"			{ loc.lines(yyleng); loc.step(); if (shouldIndent()) { return meat::yy::parser::make_Indent(loc); } }
"\r\t"			{ loc.lines(yyleng); loc.step(); if (shouldIndent()) { return meat::yy::parser::make_Indent(loc); } }
"--"[^n]*\n		{ }
[ \t\r\n\f]		{ loc.step(); }

"{-"                    { BEGIN(COMMENT_BLOCK); }
<COMMENT_BLOCK>[\n\r]	{}
<COMMENT_BLOCK>.	{}
<COMMENT_BLOCK>"-}"	{ BEGIN(INITIAL); }

"module"		{ shouldIndent(true); return meat::yy::parser::make_Module(loc); }
"imports"		{ shouldIndent(true); return meat::yy::parser::make_Imports(loc); }
"include"		{ return meat::yy::parser::make_Include(loc); }
"import"		{ return meat::yy::parser::make_Import(loc); }
"where"			{ return meat::yy::parser::make_Where(loc); }
"enum"			{ shouldIndent(true); return meat::yy::parser::make_Enum(loc); }
"record"		{ shouldIndent(true); return meat::yy::parser::make_Record(loc); }
"extend"		{ return meat::yy::parser::make_Extend(loc); }
"fix"			{ return meat::yy::parser::make_Fix(loc); }
"tag"			{ return meat::yy::parser::make_Tag(loc); }
"grp"			{ return meat::yy::parser::make_Grp(loc); }
"as"			{ return meat::yy::parser::make_As(loc); }
						
"string"		{ return meat::yy::parser::make_StringType(loc); }
			
"uint8_t"		{ return meat::yy::parser::make_UInt8Type(loc); }
"uint16_t"		{ return meat::yy::parser::make_UInt16Type(loc); }
"uint32_t"		{ return meat::yy::parser::make_UInt32Type(loc); }
"uint64_t"		{ return meat::yy::parser::make_UInt64Type(loc); }
			
"int8_t"		{ return meat::yy::parser::make_Int8Type(loc); }
"int16_t"		{ return meat::yy::parser::make_Int16Type(loc); }
"int32_t"		{ return meat::yy::parser::make_Int32Type(loc); }
"int64_t"		{ return meat::yy::parser::make_Int64Type(loc); }

"bool"			{ return meat::yy::parser::make_BoolType(loc); }
"char"			{ return meat::yy::parser::make_CharType(loc); }
"float"			{ return meat::yy::parser::make_FloatType(loc); }
"double"		{ return meat::yy::parser::make_DoubleType(loc); }
			
"{"                     { return meat::yy::parser::make_LeftBrace(loc); }
"}"			{ return meat::yy::parser::make_RightBrace(loc); }
"["			{ return meat::yy::parser::make_LeftBracket(loc); }
"]"			{ return meat::yy::parser::make_RightBracket(loc); }
"("			{ return meat::yy::parser::make_LeftParen(loc); }
")"			{ return meat::yy::parser::make_RightParen(loc); }
"|"			{ return meat::yy::parser::make_Pipe(loc); }
"="			{ return meat::yy::parser::make_Equal(loc); }
"::"			{ return meat::yy::parser::make_IsOfType(loc); }
":"			{ return meat::yy::parser::make_Colon(loc); }
";"			{ return meat::yy::parser::make_SemiColon(loc); }
"@"			{ return meat::yy::parser::make_Arobaz(loc); }
","			{ return meat::yy::parser::make_Comma(loc); }
"."			{ return meat::yy::parser::make_Dot(loc); }

[0-9]+_tag { return meat::yy::parser::make_FixTagVal(std::stoul(get_string(4)), loc); }
			
[0-9]+[uU]{1}8  { return meat::yy::parser::make_UInt8Val((uint8_t)std::stoul(get_string(2)), loc); }
[0-9]+[uU]{1}16 { return meat::yy::parser::make_UInt16Val((uint16_t)std::stoul(get_string(3)), loc); }
[0-9]+[uU]{1}32 { return meat::yy::parser::make_UInt32Val((uint32_t)std::stoul(get_string(3)), loc); }
[0-9]+[uU]{1}64 { return meat::yy::parser::make_UInt64Val((uint64_t)std::stoul(get_string(3)), loc); }
		
[0-9]+[iI]{1}8 { return meat::yy::parser::make_Int8Val((int8_t)std::stol(get_string(2)), loc); }
[0-9]+[iI]{1}16 { return meat::yy::parser::make_Int16Val((int16_t)std::stol(get_string(3)), loc); }
[0-9]+[iI]{1}32 { return meat::yy::parser::make_Int32Val((int32_t)std::stol(get_string(3)), loc); }
[0-9]+[iI]{1}64 { return meat::yy::parser::make_Int64Val((int64_t)std::stol(get_string(3)), loc); }

[0-9]+\.[0-9]*[fF]{1} { return meat::yy::parser::make_FloatVal(std::stof(get_string(1)), loc); }
[0-9]+\.[0-9]*[dD]{1} { return meat::yy::parser::make_DoubleVal(std::stod(get_string(1)), loc); }

[0-9]+ { return meat::yy::parser::make_Int64Val((int64_t)std::stol(get_string(0)), loc); }		
		
"false"		{ return meat::yy::parser::make_BoolVal(false, loc); }
"true"		{ return meat::yy::parser::make_BoolVal(true, loc); }
'(\\.|.)'	{ return meat::yy::parser::make_CharVal(yytext[0], loc); }

[a-zA-Z_][a-zA-Z_0-9]* { return meat::yy::parser::make_Identifier(get_string(0), loc); }
\"(\\.|[^"])*\" { return meat::yy::parser::make_StringVal(get_string(0), loc); }

.			{ loc.step(); }

%%			

namespace meat {
    void driver::begin() {
	yy_flex_debug = trace_parsing;
	if (filenames.empty() or filenames.back() == "-")
	    yyin = stdin;
	else if (not (yyin = fopen(filenames.back().c_str(), "r"))) {
            error("cannot open " + filenames.back());
	    std::exit(-1);
	}
    }

    void driver::end() {
	fclose(yyin);
    }
}
