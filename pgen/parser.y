%skeleton "lalr1.cc"
%require "3.0.4"

%defines

%define parser_class_name { parser }
%define parse.assert
%define parse.trace
%define parse.error verbose

%define api.token.constructor
%define api.value.type variant
%define api.namespace { meat::yy }
/* %define api.prefix { meat_ } */

%code requires
{
#include <string>
namespace meat {
    class driver;
} // namespace meat
#include <smunix/meat/semantics.hh>
}

%param { meat::driver& drv }
%locations
%initial-action
{
    // initialize the initial location
    /* @$.begin.filename = @$.end.filename = drv.file(); */
    @$.initialize(&drv.filenames.back());
}

%code
{
#include <smunix/meat/driver.hh>
extern bool shouldIndent();
extern void shouldIndent(bool);
}
			
%token END 0				"end of file"

%token	Module				"module"
%token  Include				"include"
%token  Import				"import"
%token  Imports				"imports"
%token  Where				"where"
%token  Enum				"enum"
%token  Record				"record"
%token  Extend				"extend"			
%token  Fix				"fix"
%token  Grp				"grp"
%token  Indent				"indent"			
%token  As				"as"
						
%token  StringType			"string"
			
%token	UInt8Type			"uint8_t"
%token 	UInt16Type			"uint16_t"
%token 	UInt32Type			"uint32_t"
%token 	UInt64Type			"uint64_t"
			
%token 	Int8Type			"int8_t"
%token 	Int16Type			"int16_t"
%token 	Int32Type			"int32_t"
%token 	Int64Type			"int64_t"

%token  BoolType			"bool"
%token  CharType			"char"
%token 	FloatType			"float"
%token 	DoubleType			"double"
			
%token  <std::string> Identifier	"id"
%token  <std::string> StringVal		"stringv"
			
%token	<uint8_t> UInt8Val		"uint8_tv"
%token 	<uint16_t> UInt16Val		"uint16_tv"
%token 	<uint32_t> UInt32Val		"uint32_tv"
%token 	<uint64_t> UInt64Val		"uint64_tv"
			
%token 	<int8_t> Int8Val		"int8_tv"
%token 	<int16_t> Int16Val		"int16_tv"
%token 	<int32_t> Int32Val		"int32_tv"
%token 	<int64_t> Int64Val		"int64_tv"

%token	<bool> BoolVal			"boolv"
%token	<char> CharVal			"charv"
%token 	<float> FloatVal		"floatv"
%token 	<double> DoubleVal		"doublev"

%token	<uint16_t> FixTagVal		"fixtagv"
%token	Tag				"tag"
			
%token LeftBrace			"{"
%token RightBrace			"}"
%token LeftBracket			"["
%token RightBracket			"]"
%token LeftParen			"("
%token RightParen			")"
%token Pipe				"|"
%token Equal				"="
%token IsOfType				"::"
%token Colon				":"
%token SemiColon			";"
%token Arobaz				"@"
%token Comma				","
%token Dot				"."

%type   <std::string> ELabel EScopedIds
%type   <meat::ScalarType::sp> EScalarType EArithType ESufType
%type   <meat::TypeSfxQual::sp> ETypeSufQual
%type	<meat::TypePrefix::sp> ETypePref
%type	<meat::TypeDefinition::sp> ETypeDef
%type	<meat::Field::sp> EField EExtendField
%type	<smunix::sp<meat::Field::Map>> EFields EFieldsBlock EExtendFields EExtendBlockTail EExtendBlock
%type	<meat::Record::sp> ERecord
%type	<meat::Enum::Entry::sp> EEnumItem
%type	<smunix::sp<meat::Enum::Entry::Map>> EEnumItems
%type	<meat::Enum::sp> EEnumeration
%type	<meat::Definition::sp> EDefinition
%type	<smunix::sp<meat::Definition::Map>> EDefinitions
%type	<meat::Module::sp> EModule
%type	<smunix::sp<meat::Module::Map>> EModulesBlock
%type	<meat::Import::sp> EImport
%type	<smunix::sp<meat::Import::Set>> EImports EImportsBlock
%type	<meat::File::sp> afile
																		
%start afile;
			
%printer { yyoutput << $$; } <*>

%%

afile[fl]:		EImportsBlock[aimptxs] EModulesBlock[amdlxs] { $fl = meat::alloc<meat::File>($aimptxs, $amdlxs); drv.set($fl); }
	;

EImportsBlock[imptxs]:	"imports" "{" EImports[aimptxs] "}"	{ shouldIndent(false); $imptxs = $aimptxs; }
	|	/* empty */					{ $imptxs = meat::alloc<meat::Import::Set>(); }
	;

EImports[imptxs]:	EImports[aimptxs] EImport[aimpt]	{ $imptxs = $aimptxs; $imptxs->emplace($aimpt); }
        |	EImport[aimpt]					{ $imptxs = meat::alloc<meat::Import::Set>(); $imptxs->emplace($aimpt); }
        ;

EImport[impt]:	"indent" "include" "stringv"[afln] ";" { $impt = meat::alloc<meat::Import>($afln); }
        ;

EModulesBlock[mdlxs]:	EModulesBlock[amdlxs] EModule[amdl]	{ $mdlxs = $amdlxs; smunix::asr($mdlxs)[$amdl->id()]=$amdl; }
        |	EModule[amdl]					{ $mdlxs = meat::alloc<meat::Module::Map>(); smunix::asr($mdlxs)[$amdl->id()] = $amdl; }
	;

EIndentsPlus:	EIndentsPlus "indent"
	|	"indent"
	;

EModule[mdl]:	"module" "id"[aid] "where" "{" EDefinitions[adefxs] EIndentsPlus "}"			{ shouldIndent(false); $mdl = meat::alloc<meat::Module>($aid, $adefxs); }
	|	"module" "id"[aid] "where" EIndentsPlus "{" EDefinitions[adefxs] EIndentsPlus "}"	{ shouldIndent(false); $mdl = meat::alloc<meat::Module>($aid, $adefxs); }
	|	"module" "id"[aid] "where" "{" EDefinitions[adefxs] "}"					{ shouldIndent(false); $mdl = meat::alloc<meat::Module>($aid, $adefxs); }
	|	"module" "id"[aid] "where" EIndentsPlus "{" EDefinitions[adefxs] "}"			{ shouldIndent(false); $mdl = meat::alloc<meat::Module>($aid, $adefxs); }
        ;

EDefinitions[defxs]:	EDefinitions[adefxs] EDefinition[adef]	{ $defxs = $adefxs; smunix::asr($defxs)[$adef->id()] = $adef; }
|	EDefinition[adef]					{ $defxs = meat::alloc<meat::Definition::Map>(); smunix::asr($defxs)[$adef->id()] = $adef; }
        ;

EDefinition[def]:    EEnumeration[aenm]		{ $def = $aenm; }
	|	ERecord[arec]			{ $def = $arec; }
	|	EIndentsPlus EModule[amdl]	{ $def = $amdl; }
        ;

EEnumeration[enm]:  "indent" "enum" "id"[aid] EIndentsPlus "{" EEnumItems[aenmixs] "indent" "}" { shouldIndent(false); $enm = meat::alloc<meat::Enum>($aid, $aenmixs); }
	|      "indent" "enum" "id"[aid] "{" EEnumItems[aenmixs] "indent" "}"			{ shouldIndent(false); $enm = meat::alloc<meat::Enum>($aid, $aenmixs); }
        ;

EEnumItems[enmixs]:     EEnumItems[aenmixs] EEnumItem[aenmi]	{ smunix::asr($aenmixs)[$aenmi->label] = $aenmi; $enmixs = $aenmixs;  }
        |	EEnumItem[aenmi]				{ $enmixs= meat::alloc<meat::Enum::Entry::Map>(); smunix::asr($enmixs)[$aenmi->label] = $aenmi; }
        ;

EEnumItem[enmi]:     "indent" "|" "id"[id] "=" "uint8_tv"[val] { $enmi = meat::alloc<meat::Enum::Entry>($id, $val); }
	|	"indent" "|" "id"[id] "=" "int64_tv"[val] { $enmi = meat::alloc<meat::Enum::Entry>($id, $val); }
        ;

ERecord[rec]:       "indent" "record" "id"[id] EExtendBlock[axtd] EFieldsBlock[afldxs]	{ shouldIndent(false); $rec = meat::alloc<meat::Record>($id, $afldxs, $axtd); }
	|      "indent" "record" "id"[id] EFieldsBlock[afldxs]				{ shouldIndent(false); $rec = meat::alloc<meat::Record>($id, $afldxs); }
        ;

EExtendBlock[blk]:  ":" "extend" EExtendBlockTail[blkt]		{ $blk = $blkt; }
	|      ":" "extend" EIndentsPlus EExtendBlockTail[blkt] { $blk = $blkt; }
	;

EExtendBlockTail[tl]: "{" EExtendFields[aefldxs] "}" { $tl = $aefldxs; }
	;

EExtendFields[efldxs]: EExtendFields[aefldxs] "," EExtendField[aefld]	{ smunix::asr($aefldxs)[$aefld->label] = $aefld; $efldxs = $aefldxs; }
        |      EExtendField[aefld]					{ $efldxs = meat::alloc<meat::Field::Map>(); smunix::asr($efldxs)[$aefld->label] = $aefld; }
	;

EExtendField[efld]:	EIndentsPlus EScopedIds[escidxs] "as" "id"[lbl] { $efld = meat::alloc<meat::Field>($lbl, meat::alloc<meat::TypeDefinition>(meat::alloc<ScalarTypePrefix>(meat::alloc<meat::ScalarType>($escidxs)))); }
|	EScopedIds[escidxs] "as" "id"[lbl]				{ $efld = meat::alloc<meat::Field>($lbl, meat::alloc<meat::TypeDefinition>(meat::alloc<ScalarTypePrefix>(meat::alloc<meat::ScalarType>($escidxs)))); }
	;

EScopedIds[scids]:	EScopedIds[lhs] "." "id"[rhs]	{ $scids = $lhs + "." + $rhs; }
	|	"id"[id]				{ $scids = $id; }
	;
/* ugly and hideous, yes ! */
EFieldsBlock[fldblkxs]:  "{" EFields[fldxs] "indent" "}"	{ $fldblkxs = $fldxs; }
	|      "indent" "{" EFields[fldxs] "indent" "}"		{ $fldblkxs = $fldxs; }
	|      "{" EFields[fldxs] "}"				{ $fldblkxs = $fldxs; }
	;

EFields[fldxs]:       EFields[afldxs] EField[fld]	{ $fldxs = $afldxs; smunix::asr($fldxs)[$fld->label] = $fld; }
        |      EField[fld]				{ $fldxs = meat::alloc<meat::Field::Map>(); smunix::asr($fldxs)[$fld->label] = $fld; }
        ;

EField[fld]:        "indent" ELabel[lbl] "::" ETypeDef[tpd] ";" { $fld = meat::alloc<meat::Field>($lbl, $tpd); }

ETypeDef[tpd]:	ETypePref[p] ETypeSufQual[s] { $tpd = meat::alloc<TypeDefinition>($p, $s);  }
	;

ETypePref[prf]:	EScalarType[tp]					{ $prf = meat::alloc<meat::ScalarTypePrefix>($tp); }
        |	"[" ETypePref[tp] "]"				{ $prf = meat::alloc<meat::ReferencedTypePrefix>(meat::alloc<meat::DynArrTypePrefix>($tp)); }
	|	"[" ETypePref[tp] "|" "uint8_tv"[sz]  "]"	{ $prf = meat::alloc<meat::FixArrTypePrefix>($tp, $sz); }
	|	"[" ETypePref[tp] "|" "int64_tv"[sz]  "]"	{ $prf = meat::alloc<meat::FixArrTypePrefix>($tp, $sz); }
	|	"(" ETypePref[tp] ")"				{ $prf = meat::alloc<meat::OptionalTypePrefix>($tp); }
	|	"@" ETypePref[tp]				{ $prf = meat::alloc<meat::ReferencedTypePrefix>($tp); }
	;

EArithType[res]:	"uint8_t"	{ $res = meat::alloc<meat::ScalarType>("uint8_t"); }
	|	"uint16_t" { $res = meat::alloc<meat::ScalarType>("uint16_t"); }
	|	"uint32_t" { $res = meat::alloc<meat::ScalarType>("uint32_t"); }
	|	"uint64_t" { $res = meat::alloc<meat::ScalarType>("uint64_t"); }
	|	"int8_t" { $res = meat::alloc<meat::ScalarType>("int8_t"); }
	|	"int16_t" { $res = meat::alloc<meat::ScalarType>("int16_t"); }
	|	"int32_t" { $res = meat::alloc<meat::ScalarType>("int32_t"); }
	|	"int64_t" { $res = meat::alloc<meat::ScalarType>("int64_t"); }
	|	"float" { $res = meat::alloc<meat::ScalarType>("float"); }
	|	"double" { $res = meat::alloc<meat::ScalarType>("double"); }
	;

ESufType[bfxtp]:	"string"	{ $bfxtp = meat::alloc<meat::ScalarType>("string"); }
	|	"char"		{ $bfxtp = meat::alloc<meat::ScalarType>("char"); }
	|	"bool"		{ $bfxtp = meat::alloc<meat::ScalarType>("bool"); }
	|	EArithType[art]		{ $bfxtp = $art; }
	;

EScalarType[res]:	"char"	{ $res = meat::alloc<meat::ScalarType>("char"); }
	|	"bool"		{ $res = meat::alloc<meat::ScalarType>("bool"); }
        |	EScopedIds[tp]		{ $res = meat::alloc<meat::ScalarType>($tp); }
	|	EArithType[tp]		{ $res = $tp; }
	;

ETypeSufQual[sfxQl]:	"(" "tag" ":" "uint16_tv"[tagnb] "::" ESufType[fixtp] ")"	{ $sfxQl = meat::alloc<meat::TypeSfxQualTag>($tagnb, $fixtp); }
	|	"(" "tag" ":" "int64_tv"[tagnb] "::" ESufType[fixtp] ")"	{ $sfxQl = meat::alloc<meat::TypeSfxQualTag>($tagnb, $fixtp); }
	|	"(" "grp" ":" "uint16_tv"[tagnb] ")"					{ $sfxQl = meat::alloc<meat::TypeSfxQualGrp>($tagnb); }
	|	"(" "grp" ":" "int64_tv"[tagnb] ")"					{ $sfxQl = meat::alloc<meat::TypeSfxQualGrp>($tagnb); }
        |	/* empty */								{ $sfxQl = meat::alloc<meat::TypeSfxQual>(); }
	;

ELabel[res]:		"id"[id] { $res =  $id; }
	;

%%

namespace meat { namespace yy {
	void parser::error(parser::location_type const& loc, std::string const& msg) {
	    drv.error(loc, msg);
	}
    }
}
