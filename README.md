## meat - Messaging for Electronic and Automated Transport

Meat is a language I designed to help specify quickly protocol of different types. As such, a lot of freedom is left to the end users to add their own version management on top of meat, or any other sort house keeping headers in the protocol they are designing.

As an intermediate language, it comes with Grill, sometime called meatc; i.e. meat compiler. Write down your messages using meat syntax specifications in a text file, then pass that text file to grill and it will generate a cpp version of the protocol description. Python bindings are still being worked on.

But before you get to using grill with your meat specifications, you ought to be able to download the sources, and then compile grill first. That's what I will be explaining in the next paragraph.

## Install

Meat uses [waf](https://waf.io) as its build system. Therefore, when you download the sources, you will have to initialize the (git) submodule corresponding to Waf.

Download meat

    git clone https://github.com/smunix/meat.git
    cd meat

Download Waf

    cd waf.git
    git submodule init
    git submodule update

Unpack Waf

    ./waf-light configure build
    cd ../
    alias waf='./waf'

Build Grill

    waf configure build -d release

See this link for the detailed output, [Compiling-Meat](https://github.com/smunix/meat/wiki/Compiling-Meat)
Most important is the location of the grill compiler,

    [31/35] Linking build/scratch/grill/grill

Note the path to grill has you will need it in order to generate c++ code from your meat specifications.

## Samples and Examples

A few samples can be found at

* [m1.meat](https://github.com/smunix/meat/blob/master/samples/meat/m1.meat)
* [m2.meat](https://github.com/smunix/meat/blob/master/samples/meat/m2.meat)
* [m3.meat](https://github.com/smunix/meat/blob/master/samples/meat/m3.meat)

Their C++ codegen are to be found at

* [m1.h](https://github.com/smunix/meat/blob/master/samples/cpp/m1.h)
* [m2.h](https://github.com/smunix/meat/blob/master/samples/cpp/m2.h)
* [m3.h](https://github.com/smunix/meat/blob/master/samples/cpp/m3.h)

For a realworld example leveraging the specification of New Order MuliLegs in FIX [MsgType=AB](http://www.onixs.biz/fix-dictionary/4.4/msgType_AB_6566.html) , a meaty description is provided at [ab.meat](https://github.com/smunix/meat/blob/master/src/meat/test/onixs.biz/meat/multi-leg/ab.meat), along with its C++ codegen file [ab.h](https://github.com/smunix/meat/blob/master/src/meat/test/onixs.biz/cpp/multi-leg/ab.h)

Below is the command that was used to generate the C++ codegen ab.h file from its meat specification file:

    build/scratch/grill/grill src/meat/test/onixs.biz/meat/multi-leg/ab.meat &> src/meat/test/onixs.biz/meat/multi-leg/ab.h

While building the project, you might have noticed a test-meat-multileg artifact being generated.

    [30/35] Linking build/src/meat/test/test-meat-multileg

This binary is an executable that reads, writes, manipulates different aspects of the ab.h codegen file. Take a look at the content of the file [multileg.cc](https://github.com/smunix/meat/blob/master/src/meat/test/multileg.cc), from which it is generated. This file contains enough examples for you to get started with meat code.

More can be found at [wiki](https://github.com/smunix/meat/wiki)
