#pragma once
#include <smunix/core/memory.hh>
#include <cstring>

namespace smunix {
  namespace check {
    struct NotBound {
      inline static void apply(size_t pos, size_t max) {
#ifndef NDEBUG	
	assert(pos < max);
#endif	
      }
    };
  } // namespace check
  // fix size array
  namespace fix {
    template<class T, size_t N, class Check = check::NotBound> struct arr {
      using arr_t = T[N];
      using check_t = Check;
      static constexpr size_t len = N;
      arr_t data;
      // accessor functions
      r<T> at(size_t i) { check_t::apply(i, N); return *(data + i); }
      r<T const> at(size_t i) const { check_t::apply(i, N); return *(data + i); }
      // assignment operators
      r<arr> operator=(p<T const> a) {
	std::memcpy(begin(), a, this->len * sizeof(T));
	return *this;
      }
      // utility functions
      size_t bsize() const { return len * sizeof(T); }
      static constexpr size_t bsize(size_t) { return len * sizeof(T); }
      static constexpr size_t size() { return len; }
      p<T> begin() { return data ; }
      p<T> end() { return data + len; }
      p<T> const begin() const { return data ; }
      p<T> const end() const { return data + len; }
    };
  }
  // dynamic size array
  namespace dyn {
    // the dynamic size arr assumes payload follows immediatly
    template<class T, class Check = check::NotBound, class Len = uint16_t> struct arr {
      using arr_t = fix::arr<T, 0>;
      using check_t = Check;
      using len_t = Len;
      
      len_t	len;
      arr_t	data;
      // ctor
      arr(size_t len) : len(len) { }
      // accessor functions
      r<T> at(size_t i) { check_t::apply(i, len); return *(data.data + i); }
      r<T const> at(size_t i) const { check_t::apply(i, len); return *(data.data + i); }
      // assignment operators
      r<arr> operator=(p<T const> a) {
	std::memcpy(begin(), a, this->len * sizeof(T));
	return *this;
      }
      // utility functions
      size_t bsize() const { return (p<uint8_t>(end()) - p<uint8_t>(this)); }
      static size_t bsize(size_t len) { arr a(len); return (p<uint8_t>(a.end()) - p<uint8_t>(addr(a))); }
      size_t size() const { return len; }
      p<T> begin() { return data.data ; }
      p<T> end() { return data.data + len; }
      p<T> const begin() const { return data.data ; }
      p<T> const end() const { return data.data + len; }
    };    
  } // namespace dyn
  namespace var {
    // at(0) is invalid on var::arr !!!
    template<class T, size_t N, class Check = check::NotBound> struct arr {
      using check_t = Check;
      using arr_t = fix::arr<T, N, check_t>;
      size_t next = 0;
      up<arr_t> data;
      // ctor
      arr() : data(new arr_t) {}
      // modifies
      void reset() { next = 0; }
      void push_back(r<T const> v) { check_t::apply(++next, arr_t::len); data->at(next) = v; }
      // accessor functions
      r<T> at(size_t i) { assert(i > 0); check_t::apply(i, next); return data->at(i); }
      r<T const> at(size_t i) const { assert(i > 0); check_t::apply(i, next); return data->at(i); }
      r<T> back() { return this->at(next); }
      r<T const> back() const { return this->at(next); }
      // utility functions
      size_t size() const { return next; }
      p<T> begin() { return data->begin() + 1 ; }
      p<T> end() { return data->begin() + next; }
      p<T> const begin() const { return data->begin() + 1 ; }
      p<T> const end() const { return data->begin() + next; }
    };
  } // namespace var
} // namespace smunix

