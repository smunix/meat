#pragma once
#include <stdexcept>
#include <smunix/core/memory.hh>

namespace smunix {
  // offset based access
  namespace off {
    namespace exception {
      struct DeRef  : public std::runtime_error {
	DeRef(r<std::string const> wht) : std::runtime_error(wht) {}
	DeRef(p<char const> wht) : std::runtime_error(wht) {}
      };
    } // namespace exception
    // the dynamic size arr assumes payload follows immediatly
    template<class T> struct Ref {
      int16_t dist = 0;
      void distance(int16_t d) {
	dist = d; // TODO : fixme, check for overflow when setting other types than int16_t!
      }
      int16_t distance() const {
	return dist;
      }
      size_t bsize() const { return sizeof(int16_t); }
      static size_t bsize(size_t len = 0) { return sizeof(int16_t); }
      operator bool () const {
	return 0 != distance();
      }
      r<Ref> set(p<uint8_t> ptee) {
	distance(ptee - asp<uint8_t>(this));
	return asr<Ref, Ref>(this);
      }
      r<T> operator* () {
	return deRef();
      }
      r<T const> operator* () const {
	return deRef();
      }
      r<T> deRef() {
	if (0 == distance())
	  throw exception::DeRef("Attempt to load unassigned ref.");
	return asr((p<T>)(asp<uint8_t>(this) + distance()));
      }
      r<T const> deRef() const {
	if (0 == distance())
	  throw exception::DeRef("Attempt to load unassigned ref.");
	return asr((p<T const>)(asp<uint8_t const>(this) + distance()));
      }
      operator r<T> () {
	return deRef();
      }
      operator r<T const> () const {
	return deRef();
      }
      friend r<T> unRef(r<Ref> ar) {
	return *ar;
      }
      friend r<T const> unRef(r<Ref const> ar) {
	return *ar;
      }
    };
    template<class T> r<T> peekRef(r<Ref<data<T>>> aRef) {
      return peek(unRef(aRef));
    }
    template<class T> r<T> peekRef(r<data<Ref<T>>> aRef) {
      return unRef(peek(aRef));
    }
    
  } // namespace off
} // namespace smunix

