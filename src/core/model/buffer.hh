#pragma once
#include <smunix/core/memory.hh>
#include <smunix/core/array.hh>
#include <smunix/core/ref.hh>

namespace smunix {
  namespace buffer {
    namespace exception {
      struct Alloc : public std::runtime_error {
	Alloc(r<std::string const> wht, size_t algn, size_t sz) : std::runtime_error(wht), algn(algn), size(sz) {}
	size_t algn;
	size_t size;
      };
      struct AllocRef : public std::runtime_error {
	AllocRef(r<std::string const> wht, size_t algn, size_t sz) : std::runtime_error(wht), algn(algn), size(sz) {}
	size_t algn;
	size_t size;
      };
      struct DoubleAllocRef : public std::runtime_error {
	DoubleAllocRef(r<std::string const> wht) : std::runtime_error(wht) {}
	DoubleAllocRef(p<char const> wht) : std::runtime_error(wht) {}
      };
      struct Cast : public std::runtime_error {
	Cast(r<std::string const> wht, size_t algn, size_t sz) : std::runtime_error(wht), algn(algn), size(sz) {}
	size_t algn;
	size_t size;
      };
    } // namespace exception

    enum class AlignType {
      WITH_ALIGN,
	WITHOUT_ALIGN,
    };
    
    namespace sz {
      template<class T> struct Bytes {
	static constexpr size_t size() { return sizeof(T); }
      };
      template<class T, AlignType ALN> struct Align {
	static constexpr size_t apply() { return alignof(T); }	
      };
      template<class T> struct Align<T, AlignType::WITHOUT_ALIGN> {
	static constexpr size_t apply() { return 1; }	
      };
      
      // specialization for Ref'ed objects
      template<class T> struct Bytes<off::Ref<T>> {
	static size_t size() { return off::Ref<T>::bsize(0); }
      };
      template<class T, AlignType ALN> struct Align<off::Ref<T>, ALN> {
	static constexpr size_t apply() { return alignof(off::Ref<T>); }
      };      
      template<class T> struct Align<off::Ref<T>, AlignType::WITHOUT_ALIGN> {
	static constexpr size_t apply() { return 1; }
      };      
      // specialization for  dynamic array
      template<class T> struct Bytes<dyn::arr<T>> {
	static size_t size(size_t len) { return dyn::arr<T>::bsize(len); }
      };
      template<class T, AlignType ALN> struct Align<dyn::arr<T>, ALN> {
	static constexpr size_t apply() { return alignof(dyn::arr<T>); }
      };
      template<class T> struct Align<dyn::arr<T>, AlignType::WITHOUT_ALIGN> {
	static constexpr size_t apply() { return 1; }
      };
      // specialization for  fix array
      template<class T, size_t N> struct Bytes<fix::arr<T, N>> {
	static size_t size() { return fix::arr<T, N>::bsize(0); }
      };
      template<class T, size_t N, AlignType ALN> struct Align<fix::arr<T, N>, ALN> {
	static constexpr size_t apply() { return alignof(fix::arr<T, N>); }
      };
      template<class T, size_t N> struct Align<fix::arr<T, N>, AlignType::WITHOUT_ALIGN> {
	static constexpr size_t apply() { return 1; }
      };
    } // namespace sz

    namespace mem {
      template<AlignType ALGN = AlignType::WITH_ALIGN> struct Align;

      template<> struct Align<AlignType::WITHOUT_ALIGN> {
	static p<void> apply(size_t alignment, size_t size, r<p<void>> rp, r<size_t> rspace) {
	  p<uint8_t> lrp = p<uint8_t>(rp);
	  p<uint8_t> laligned_rp = p<uint8_t>(((size_t)lrp + alignment - 1) & (- alignment));
	  size_t alignDistance = laligned_rp - lrp;
	  if (size + alignDistance > rspace)
	    return nullptr;
	  rspace -= alignDistance;
	  return (rp = p<void>(laligned_rp));
	}
      };
      
      template<> struct Align<AlignType::WITH_ALIGN> {
	static p<void> apply(size_t alignment, size_t size, r<p<void>> rp, r<size_t> rspace) {
#if 0
	  // Most of gcc versions (4.8, 4.9) still do not implement std::align
	  return std::align(alignment, size, rp, rspace);
#else
	  return Align<AlignType::WITHOUT_ALIGN>::apply(alignment, size, rp, rspace);
#endif	  
	}
      };
      template<AlignType ALGN> p<void> align(size_t alignment, size_t size, r<p<void>> rp, r<size_t> rspace) {
	return Align<ALGN>::apply(alignment, size, rp, rspace);
      }
    } // namespace mem
    
    template<class B = uint8_t, uint16_t N = 1 << 12, AlignType ALGN = AlignType::WITH_ALIGN> struct alignas(64) Message {
      using Arr = fix::arr<B, N>;
      using Elem = B;
      static_assert(sizeof(Elem) == 1, "size of elem in buffer is not 1 byte!");
      static constexpr AlignType alignType() { return ALGN; }
      
      struct Header {
	uint16_t next;
	uint16_t len;
	// utility functions
	p<uint8_t> begin() {
	  return p<uint8_t>(this);
	}
	p<uint8_t> end() {
	  return p<uint8_t>(this + 1) + next;
	}
	// alloc sz bytes
	p<void> bAlloc(r<bool> rc, r<Arr> data, size_t algn, uint16_t sz) {
	  p<void> buf = addr(data.at(next));
	  p<void> orig_buf = buf;
	  size_t avail = len - next;
	  if (mem::align<Message::alignType()>(algn, sz, buf, avail))
	    {
	      next += (sz + (asp<uint8_t>(buf) - asp<uint8_t>(orig_buf)));
	      rc = true;
	      return buf;
	    }
	  rc = false;
	  return nullptr;
	}
	// cast sz bytes ?
	p<void> bCast(r<bool> rc, r<Arr> data, size_t algn, uint16_t sz) {
	  p<void> buf = addr(data.at(0));
	  p<void> orig_buf = buf;
	  size_t avail = len - 0;
	  if (mem::align<Message::alignType()>(algn, sz, buf, avail))
	    {
	      next += (sz + (asp<uint8_t>(buf) - asp<uint8_t>(orig_buf)));
	      rc = true;
	      return buf;
	    }
	  rc = false;
	  return nullptr;
	}	
      };

      Header	header;
      Arr	data;
      // ctor;
      Message() : header{ 0, Arr::bsize(0) } { reset(); }
      void reset() {
	header = Header{ 0, Arr::bsize(0) };
	std::memset(data.begin(), 0, data.end() - data.begin());
      }
      // cast or get
      template<class T, class ...A> r<T> cast(r<bool> rc, A&& ...a) {
	p<void> rp = header.bCast(rc, data, sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));
	if (rc and rp)
	  return asr<T>(rp);
	throw exception::Cast("Failed to Cast bytes from buffer.", sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));
      }
      template<class T, class ...A> r<T const> cast(r<bool> rc, A&& ...a) const {
	p<void> rp = header.bCast(rc, data, sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));
	if (rc and rp)
	  return asr<T const>(rp);
	throw exception::Cast("Failed to Cast bytes from buffer.", sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));
      }
      // allocate fields
      template<class T, class ...A> r<T> alloc(r<bool> rc, A&& ...a) {
	p<void> rp = header.bAlloc(rc, data, sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));
	if (rc and rp) {
	  new (rp) T(std::forward<A>(a)...);
	  return asr<T>(rp);
	}
	throw exception::Alloc("Failed to allocate bytes from buffer.", sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));
      }
      // allocate ref'ed fields
      template<class T, class ...A> r<off::Ref<T>> allocRef(r<bool> rc, r<off::Ref<T>> rf, A&& ...a) {
	if (rf) {
	  rc = false;
	  throw exception::DoubleAllocRef("Attempt to realloc a Ref");
	}
	p<void> rp = header.bAlloc(rc, data, sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));
	if (rc and rp) {
	  new (rp) T(std::forward<A>(a)...);
	  return rf.set(asp<uint8_t>(rp));
	}
	throw exception::AllocRef("Failed to allocate bytes from buffer for Ref.", sz::Align<T, alignType()>::apply(), sz::Bytes<T>::size(std::forward<A>(a)...));	
      }
    };
  } // namespace buffer
} // namespace smunix
