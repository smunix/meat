#pragma once
#include <memory>
namespace smunix {
  template<class T> using p = T*;
  template<class T> using sp = std::shared_ptr<T>;
  template<class T> using up = std::unique_ptr<T>;
  template<class T> using r = T&;
  template<class T> struct data {
    r<T const> asr() const {
      return f;
    }
    r<T> asr() {
      return f;
    }
    r<T> operator* () {
      return asr();
    }
    r<T const> operator* () const {
      return asr();
    }
    void operator=(r<T const> af) {
      f = af;
    }
    operator r<T> () {
      return f;
    }
    operator r<T const> () const {
      return f;
    }
    friend r<T> peek(r<data> dt) {
      return *dt;
    }
    friend r<T const> peek(r<data const> dt) {
      return *dt;
    }
    T f;
  };
  // allocation utilities / shall we work on allocators ?
  template<class T, class ...Axs> sp<T> spAlloc(Axs&& ...axs) {
    return std::make_shared<T>(std::forward<Axs>(axs)...);
  }
  // address of
  template<class T> p<void> addr(r<T> ar) { return std::addressof(ar); }
  // ref conversion
  template<class T> r<T> asr(r<T> ar) { return ar; }
  template<class T> r<T> asr(p<T> ap) { return *ap; }
  template<class T> r<T> asr(sp<T> ap) { return *ap; }
  template<class T> r<T> asr(void* ap) { return asr<T>(p<T>(ap)); }
  template<class T, class U> r<T> asr(p<U> ap) { return asr<T>((p<void>)ap); }
  template<class T, class U> r<T> asr(r<U> ar) { return asr<T>(addr(ar)); }
  
  // pointer manip
  template<class T, class U> p<T> asp(p<U> ap) { return (p<T>)ap; }
  template<class T, class U> sp<T> asp(sp<U> ap) { return std::static_pointer_cast<T>(ap); }
}


