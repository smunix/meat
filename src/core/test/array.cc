#include <boost/test/unit_test.hpp>
#include <smunix/core-module.hh>

BOOST_AUTO_TEST_CASE(test_dummy)
{
  BOOST_CHECK_MESSAGE(1==1, "1 EQ 1");
}
  
struct A {
  char			c   = 'Z';
  uint32_t		v   = 0xbadbeef;
  smunix::p<bool>	del = nullptr;
  A() {}
  A(smunix::p<bool> del) : del(del) {}
  ~A() { if (del) *del = true; }
};

struct Data {
  smunix::data<A>                                      aField;  // strict allocation
  smunix::data<smunix::off::Ref<A>>                    rField;  // lazy allocation
  smunix::data<smunix::fix::arr<A, 20>>                fixGrp;	// strict allocation
  smunix::data<smunix::off::Ref<smunix::dyn::arr<A>>>  dynGrp;	// lazy dynamic allocation (dyn::arr must be allocated in a lazy fashion only. Enforce this in the grammar syntax)
  smunix::data<smunix::fix::arr<A, 40>>                fixGrp0;	// strict allocation
  smunix::data<smunix::fix::arr<A, (1<<4)>>            fixGrp1;	// strict allocation, no memory breach
  smunix::data<smunix::off::Ref<smunix::fix::arr<A, (1<<10)>>> fixGrp2;	// lazy allocation, no memory breach yet

  // utilitity functions
  size_t bsize() const { return sizeof(Data); }
  static constexpr size_t bsize(size_t = 0) { return sizeof(Data); }
};

BOOST_AUTO_TEST_CASE(test_array_fix_default_ctor)
{
  using namespace smunix;
  constexpr size_t N = 20;
  fix::arr<bool, N> bxs;
  uint8_t i = 0;
  {
    fix::arr<A, N> axs;
    for(auto &e : axs) {
      e.del = std::addressof(bxs.at(i));
      e.v = ++i;
    }
    i = 0;
    for(auto &e : axs)
      BOOST_CHECK_MESSAGE(e.v == ++i, "access member in array");
  }
  for(auto &b : bxs)
    BOOST_CHECK_MESSAGE(b, "array member deleted");
}

BOOST_AUTO_TEST_CASE(test_array_fix_custom_ctor)
{
  using namespace smunix;
  constexpr size_t N = 20;
  fix::arr<bool, N> bxs;
  uint8_t i = 0;
  {
    fix::arr<sp<A>, N> axs;
    for(auto &e : axs) {
      e = std::make_shared<A>(std::addressof(bxs.at(i)));
      e->v = ++i;
    }
    i = 0;
    for(auto &e : axs)
      BOOST_CHECK_MESSAGE(e->v == ++i, "access member in array");
  }
  for(auto &b : bxs)
    BOOST_CHECK_MESSAGE(b, "array member deleted");
}

BOOST_AUTO_TEST_CASE(test_array_dyn)
{
  using namespace smunix;
  using Msg = buffer::Message<uint8_t, (1 << 12)>; // 4K

  Msg msg;
  bool success = false;
  // the following is using a Message as a buffer, which is not 100% correct. 
  msg.alloc<fix::arr<A,20>>(success); // allocate 1 fix array with 20 As
  BOOST_CHECK_MESSAGE(success, "alloc 1 fix array of As");
  msg.alloc<dyn::arr<A>>(success, 20); // allocate 1 dyn array with 20 As
  BOOST_CHECK_MESSAGE(success, "alloc 1 dynamic array of As");
  msg.alloc<fix::arr<A,40>>(success); // allocate 1 fix array with 20 As
  BOOST_CHECK_MESSAGE(success, "alloc 1 fix array of As");
  msg.alloc<fix::arr<A,(1<<4)>>(success); // allocate 1 fix array with 16 As
  BOOST_CHECK_MESSAGE(success, "alloc 1 fix array of As");
  msg.alloc<off::Ref<fix::arr<A,(1<<10)>>>(success); // allocate ref to 1 fix array with 1024 As. 
  BOOST_CHECK_MESSAGE(success, "alloc 1 Ref of fix array of As");
  r<Data> data = msg.alloc<Data>(success); // allocate ref to 1 fix array with 1024 As. 
  BOOST_CHECK_MESSAGE(success, "alloc 1 Data");
  msg.allocRef(success, peek(data.dynGrp), 10);
  BOOST_CHECK_EXCEPTION(msg.allocRef(success, peek(data.dynGrp), 10), buffer::exception::DoubleAllocRef, [](r<buffer::exception::DoubleAllocRef const>) -> bool { return true; });
}

BOOST_AUTO_TEST_CASE(test_data_struct)
{
  using namespace smunix;
  using Msg = buffer::Message<uint8_t, (1 << 12)>; // 4K

  Msg msg;
  bool success = false;
  r<Data> data = msg.alloc<Data>(success); // allocate ref to 1 fix array with 1024 As.
  BOOST_CHECK_MESSAGE(success, "alloc 1 Data");
  BOOST_CHECK_MESSAGE(peek(data.aField).v == 0xbadbeef, "A.v field successfully initialized");
  BOOST_CHECK_MESSAGE(not peek(data.rField), "rField ref not allocated yet");
  BOOST_CHECK_EXCEPTION(unRef(peek(data.rField)), off::exception::DeRef, [] (r<off::exception::DeRef const>) -> bool { return true; });  
  msg.allocRef(success, peek(data.rField));
  BOOST_CHECK_MESSAGE(success, "rField successfully allocated");
  BOOST_CHECK_MESSAGE(peek(data.rField), "rField ref is now allocated");
  BOOST_CHECK_MESSAGE(unRef(peek(data.rField)).v == 0xbadbeef, "A.v ref'ed field successfully accessed");
  BOOST_CHECK_MESSAGE(0 == size_t(addr(peek(data.rField).deRef().v)) % alignof(uint32_t), "member v of ref'ed rField is well aligned");
  BOOST_CHECK_EXCEPTION(msg.allocRef(success, peek(data.fixGrp2)), buffer::exception::AllocRef, [] (r<buffer::exception::AllocRef const>) -> bool { return true; });
  BOOST_CHECK_MESSAGE(not peek(data.fixGrp2), "fixGrp2 ref not allocated yet");  
  BOOST_CHECK_MESSAGE(not peek(data.dynGrp), "dynGrp ref not allocated yet");
  msg.allocRef(success, peek(data.dynGrp), 10);
  BOOST_CHECK_MESSAGE(peek(data.dynGrp), "dynGrp ref is now allocated");
  BOOST_CHECK_EXCEPTION(msg.allocRef(success, peek(data.dynGrp), 10), buffer::exception::DoubleAllocRef, [] (r<buffer::exception::DoubleAllocRef const>) -> bool { return true; });
}

struct Composition {
  smunix::data<Data>	data;
  smunix::data<smunix::off::Ref<Data>> rData;
  smunix::data<smunix::fix::arr<Data, 5>> fixArrData;
  smunix::data<smunix::off::Ref<smunix::dyn::arr<Data>>> rDynArrData;
};

BOOST_AUTO_TEST_CASE(test_data_composition_too_small)
{
  using namespace smunix;
  using Msg = buffer::Message<uint8_t, (1 << 12)>; // 4K
  Msg msg;
  bool success = false;
  BOOST_CHECK_EXCEPTION(msg.alloc<Composition>(success),
			buffer::exception::Alloc,
			[] (r<buffer::exception::Alloc const> ex) -> bool { BOOST_TEST_MESSAGE(ex.what() << ", align=" << ex.algn << ", size=" << ex.size); return true; });
  BOOST_CHECK_MESSAGE(not success, "Failed to alloc 1 Composition");
}

BOOST_AUTO_TEST_CASE(test_data_composition_too_tight)
{
  using namespace smunix;
  using Msg = buffer::Message<uint8_t, (1 << 13)>; // 8K
  Msg msg;
  bool success = false;
  r<Composition> composition = msg.alloc<Composition>(success);
  BOOST_CHECK_MESSAGE(success, "Successfully alloc'ed 1 Composition");
  BOOST_CHECK_EXCEPTION(msg.allocRef(success, peek(composition.rData)),
			buffer::exception::AllocRef,
			[] (r<buffer::exception::AllocRef const> ex) -> bool { BOOST_TEST_MESSAGE(ex.what() << ", align=" << ex.algn << ", size=" << ex.size); return true; });
}

template<class Msg>
void Composition_data_composition_too_big(smunix::r<bool> success, smunix::r<Composition> composition, smunix::r<Msg> msg) {
  using namespace smunix;
  
  BOOST_CHECK_MESSAGE(success, "Successfully alloc'ed 1 Composition");
  BOOST_CHECK_MESSAGE(not peek(composition.rData), "Ref rData is not up");
  msg.allocRef(success, peek(composition.rData));
  BOOST_CHECK_MESSAGE(success, "Successfully lazily alloc'ed rData");
  BOOST_CHECK_MESSAGE(peek(composition.rData), "Ref rData is up");
  BOOST_CHECK_MESSAGE(not peek(composition.rDynArrData), "Ref rDynArrData is not up");  
  msg.allocRef(success, peek(composition.rDynArrData), 5);
  BOOST_CHECK_MESSAGE(peek(composition.rDynArrData), "Ref rDynArrData is up");  
  BOOST_CHECK_MESSAGE(peekRef(composition.rDynArrData).len == 5, "Valid size of dyn array check");
  BOOST_CHECK_MESSAGE(not peek(peekRef(composition.rDynArrData).at(0).rField), "rField on ref'ed 0 elem is not up yet");
  size_t i = 0;
  for (auto& d: peekRef(composition.rDynArrData)) {
    BOOST_CHECK_MESSAGE(not peek(d.rField), boost::lexical_cast<std::string>(i++) + " rField on ref'ed Data is not up yet");
  }
  msg.allocRef(success, peek(peekRef(composition.rDynArrData).at(2).rField));
  BOOST_CHECK_MESSAGE(success, "Lazily alloc a ref'ed Data in a dyn arr");
  peekRef(peekRef(composition.rDynArrData).at(2).rField).c = 'B';
  peekRef(peekRef(composition.rDynArrData).at(2).rField).v = 0xb13b00b;
  msg.allocRef(success, peek(peekRef(composition.rDynArrData).at(1).rField));
  BOOST_CHECK_MESSAGE(success, "Lazily alloc a ref'ed Data in a dyn arr");
  peekRef(peekRef(composition.rDynArrData).at(1).rField).c = 'A';
  peekRef(peekRef(composition.rDynArrData).at(1).rField).v = 0xb13b00a;
  i = 0;
  for (auto& d: peekRef(composition.rDynArrData)) {
    if (i != 1 and i != 2)
      BOOST_CHECK_MESSAGE(not peek(d.rField), boost::lexical_cast<std::string>(i) + " rField on ref'ed Data is not up yet");
    else
      BOOST_CHECK_MESSAGE(peek(d.rField), boost::lexical_cast<std::string>(i) + " rField on ref'ed Data is up now");
    if (i == 1) {
      BOOST_CHECK_MESSAGE(peekRef(peekRef(composition.rDynArrData).at(i).rField).c == 'A', "read char field");
      BOOST_CHECK_MESSAGE(peekRef(peekRef(composition.rDynArrData).at(i).rField).v == 0xb13b00a, "read uint32_t field");
    }
    if (i == 2) {
      BOOST_CHECK_MESSAGE(peekRef(peekRef(composition.rDynArrData).at(i).rField).c == 'B', "read char field");
      BOOST_CHECK_MESSAGE(peekRef(peekRef(composition.rDynArrData).at(i).rField).v == 0xb13b00b, "read uint32_t field");
    }
    i++;
  }
}

BOOST_AUTO_TEST_CASE(test_data_composition_too_big)
{
  using namespace smunix;
  using Msg = buffer::Message<uint8_t, (1 << 14)>; // 16K
  Msg msg;
  bool success = false;
  r<Composition> composition = msg.alloc<Composition>(success);
  Composition_data_composition_too_big(success, composition, msg);
}

struct Entailment : Composition {
  smunix::data<smunix::off::Ref<Data>> rData;
  smunix::data<smunix::fix::arr<smunix::off::Ref<Data>, 2>> rFixArrData;
  smunix::data<smunix::fix::arr<smunix::off::Ref<Data>, 0>> rFixArrData2;
  smunix::data<smunix::fix::arr<smunix::off::Ref<Data>, 0>> rFixArrData3;
};

template<class Msg>
void Entailment_inheritance(smunix::r<bool> success, smunix::r<Entailment> entailment, smunix::r<Msg> msg) {
  using namespace smunix;
  
  BOOST_CHECK_MESSAGE(success, "Successfully alloc'ed 1 Entailment");
  BOOST_CHECK_MESSAGE(not peek(entailment.rData), "Ref rData is not up");
  msg.allocRef(success, peek(entailment.rData));
  BOOST_CHECK_MESSAGE(success, "Successfully lazily alloc'ed rData");
  BOOST_CHECK_MESSAGE(peek(entailment.rData), "Ref rData is up");

  Composition_data_composition_too_big(success, asr<Composition>(entailment), msg); // inheritance conversion
}

BOOST_AUTO_TEST_CASE(test_inheritance)
{
  using namespace smunix;
  using Msg = buffer::Message<uint8_t, (1 << 15)>; // 32K
  Msg msg;
  bool success = false;
  r<Entailment> entailment = msg.alloc<Entailment>(success);
  Entailment_inheritance(success, entailment, msg);
}

struct Recursive {
  smunix::data<int16_t> aInt16;
  smunix::data<smunix::off::Ref<Recursive>> rSelf;
  smunix::data<smunix::off::Ref<smunix::dyn::arr<Recursive>>> rDynArr;
  smunix::data<smunix::off::Ref<smunix::fix::arr<Recursive, 10>>> rFixArr;
};

BOOST_AUTO_TEST_CASE(test_recursion)
{
  using namespace smunix;
  using Msg = buffer::Message<uint8_t, (1 << 10)>; // 1K
  Msg msg;
  bool success = false;
  r<Recursive> recursive = msg.alloc<Recursive>(success);
  BOOST_CHECK_MESSAGE(success, "strict alloc of Recursive data (through ref'ed field");

  msg.allocRef(success, peek(recursive.rFixArr));
  BOOST_CHECK_MESSAGE(success, "lazy alloc of Recursive fix arr (through ref'ed field");

  msg.allocRef(success, peek(recursive.rDynArr), 10);
  BOOST_CHECK_MESSAGE(success, "lazy alloc of Recursive dyn arr (through ref'ed field");

  msg.allocRef(success, peek(recursive.rSelf));
  BOOST_CHECK_MESSAGE(success, "lazy alloc of Recursive self type (through ref'ed field");

  BOOST_CHECK_MESSAGE(not peek(peekRef(recursive.rSelf).rSelf), "no ref'ed data allocated in recursive struct");
}
