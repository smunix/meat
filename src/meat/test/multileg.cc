#include <boost/test/unit_test.hpp>
#include <map>
#include <smunix/core-module.hh>
#include "onixs.biz/cpp/multi-leg/ab.h"

using namespace smunix;
static constexpr size_t CACHELINE = (1 << 6);
static constexpr size_t MESSAGESZ = 3*CACHELINE; // 3x cache line size

template<class Msg> void init_NewOrderMultileg(r<bool> success, r<onixs::NewOrderMultileg> noml, Msg&& msg) {
  BOOST_CHECK_MESSAGE(success, "strict alloc of NewOrderMultileg");
  msg.allocRef(success, noml.Underlyings(), 2);
  BOOST_CHECK_MESSAGE(success, "lazy alloc of 2 Underlyings");
  {
    auto& underlying = (*noml.Underlyings()).at(0);
    msg.allocRef(success, underlying.Symbol(), std::strlen("IBM")); // IBM
    BOOST_CHECK_MESSAGE(success, "Lazy alloc of Underlying Symbol IBM");
    *underlying.Symbol() = "IBM";
    msg.allocRef(success, underlying.SymbolSfx(), std::strlen("IBM.n")); // IBM.n
    BOOST_CHECK_MESSAGE(success, "Lazy alloc of Underlying SymbolSfx IBM.n");
    *underlying.SymbolSfx() = "IBM.n";
  }
  {
    auto& underlying = (*noml.Underlyings()).at(1);
    msg.allocRef(success, underlying.Symbol(), std::strlen("GOOGL")); // GOOGL
    BOOST_CHECK_MESSAGE(success, "Lazy alloc of Underlying Symbol GOOGL");
    *underlying.Symbol() = "GOOGL";
    msg.allocRef(success, underlying.SymbolSfx(), std::strlen("GOOGL.n")); // GOOGL.n
    BOOST_CHECK_MESSAGE(success, "Lazy alloc of Underlying SymbolSfx GOOGL.n");
    *underlying.SymbolSfx() = "GOOGL.n";
  }
  msg.allocRef(success, noml.InstrumentLegs(), 2);
  BOOST_CHECK_MESSAGE(success, "lazy alloc of 2 InstrumentLegs");
  {
    auto& instrmt = (*noml.InstrumentLegs()).at(0);
    msg.allocRef(success, (instrmt.Leg()).LegMaturityData());
    BOOST_CHECK_MESSAGE(success, "lazy alloc of MaturityData");
    auto& leg = instrmt.Leg();
    msg.allocRef(success, instrmt.Leg().LegSymbol(), std::strlen("IBM-STK-0")); // IBM
    *leg.LegSymbol() = "IBM-STK-0";
    auto& legMaturity = *leg.LegMaturityData();
    legMaturity.yy() = 15;
    legMaturity.mm() = 11;
    legMaturity.dd() = 24;
    instrmt.LegQty() = types::Qty {{20}};
    instrmt.LegSide() = types::Side::Buy;
    instrmt.LegSwapType() = types::SwapType::Bear;
  }
  {
    auto& instrmt = (*noml.InstrumentLegs()).at(1);
    msg.allocRef(success, instrmt.Leg().LegMaturityData());
    BOOST_CHECK_MESSAGE(success, "lazy alloc of MaturityData");
    auto& leg = instrmt.Leg();
    msg.allocRef(success, instrmt.Leg().LegSymbol(), std::strlen("GOOGL-STK-1")); // GOOGL
    *leg.LegSymbol() = "GOOGL-STK-1";
    auto& legMaturity = *leg.LegMaturityData();
    legMaturity.yy() = 15;
    legMaturity.mm() = 11;
    legMaturity.dd() = 30;
    instrmt.LegQty() = types::Qty {{30}} ;
    instrmt.LegSide() = types::Side::Sell;
    instrmt.LegSwapType() = types::SwapType::Bear;
  }
  noml.MsgType() = types::Type::AB;
  noml.ClOrdId().value() = 0xdeadbeef;
  noml.Side() = types::Side::Buy;
  
  auto& rdate = noml.TransactionTime().date();
  rdate.yy() = 15;
  rdate.mm() = 11;
  rdate.dd() = 25;
  
  auto& rtime = noml.TransactionTime().time();
  rtime.hh() = 7;
  rtime.mm() = 9;
  rtime.ss() = 52;  
}

template<class Msg> void check_init_NewOrderMultileg(r<bool> success, r<onixs::NewOrderMultileg> noml, Msg&& msg) {
  BOOST_CHECK_MESSAGE((*noml.InstrumentLegs()).len == 2, "leg numbers");
  {
    auto& instrmt = (*noml.InstrumentLegs()).at(0);
    auto& leg = instrmt.Leg();
    auto& maturity = *leg.LegMaturityData();
    BOOST_CHECK_MESSAGE(std::string((*leg.LegSymbol()).begin(), (*leg.LegSymbol()).len) == "IBM-STK-0", "LegSymbol \"IBM-STK-0\"");
    BOOST_CHECK_EQUAL(maturity.yy(), 15);
    BOOST_CHECK_MESSAGE(maturity.yy() == 15, "YY maturity date at leg 0");
    BOOST_CHECK_MESSAGE(maturity.mm() == 11, "MM maturity date at leg 0");
    BOOST_CHECK_MESSAGE(maturity.dd() == 24, "DD maturity date at leg 0");
    BOOST_CHECK_MESSAGE(instrmt.LegSide() == types::Side::Buy, "SIDE at leg 0");
  }
  {
    auto& instrmt = (*noml.InstrumentLegs()).at(1);
    auto& leg = instrmt.Leg();
    auto& maturity = *leg.LegMaturityData();
    BOOST_CHECK_MESSAGE(std::string((*leg.LegSymbol()).begin(), (*leg.LegSymbol()).len) == "GOOGL-STK-1", "LegSymbol \"GOOGL-STK-1\"");
    BOOST_CHECK_MESSAGE(maturity.yy() == 15, "YY maturity date at leg 1");
    BOOST_CHECK_MESSAGE(maturity.mm() == 11, "MM maturity date at leg 1");
    BOOST_CHECK_MESSAGE(maturity.dd() == 30, "DD maturity date at leg 1");
    BOOST_CHECK_MESSAGE(instrmt.LegSide() == types::Side::Sell, "SIDE at leg 1");
  }
  BOOST_CHECK_MESSAGE(noml.MsgType() == types::Type::AB, "Strategy MsgType");
  BOOST_CHECK_MESSAGE (noml.ClOrdId().value() == 0xdeadbeef, "Strategy ClOrdId");
  BOOST_CHECK_MESSAGE (noml.Side() == types::Side::Buy, "Strategy Side");
  
  auto& rdate = noml.TransactionTime().date();
  BOOST_CHECK_MESSAGE(rdate.yy() == 15, "Strategy year");
  BOOST_CHECK_MESSAGE(rdate.mm() == 11, "Strategy month");
  BOOST_CHECK_MESSAGE(rdate.dd() == 25, "Strategy day");
  
  auto& rtime = noml.TransactionTime().time();
  BOOST_CHECK_MESSAGE(rtime.hh() == 7, "Strategy hour");
  BOOST_CHECK_MESSAGE(rtime.mm() == 9, "Strategy month");
  BOOST_CHECK_MESSAGE(rtime.ss() == 52, "Strategy seconds");  
}

BOOST_AUTO_TEST_CASE(test_init_NewOrderMultileg)
{
  using Msg = buffer::Message<uint8_t, MESSAGESZ>;
  Msg msg;
  bool success = false;
  r<onixs::NewOrderMultileg> noml = msg.alloc<onixs::NewOrderMultileg>(success);
  init_NewOrderMultileg(success, noml, msg);
}

struct DescNewOrderMultileg {
  struct Entry {
    using Map = std::map<std::string, Entry>;
    std::string field;
    size_t offset;
    std::string type;
    operator std::string () const {
      stream::Out<std::ostringstream> os;
      os | "field=" | field | ", offset=" | offset | ", type=" | type;
      return (*os).str();
    }
  };
  template<class T> void apply(r<std::string const> field, size_t offset) {
    entries[field] = Entry{field, offset, cpp::demangle<T>()};
  }
  Entry::Map entries;
};

template<class Msg> void describe_NewOrderMultileg(r<bool> success, r<onixs::NewOrderMultileg> noml, Msg&& msg) {
  DescNewOrderMultileg d;
  onixs::NewOrderMultileg::describe(d);

  BOOST_CHECK_MESSAGE(((std::string)d.entries["MsgType"] == std::string("field=MsgType, offset=0, type=types::Type")), "MsgType check");
  BOOST_CHECK_MESSAGE(((std::string)d.entries["ClOrdId"] == std::string("field=ClOrdId, offset=8, type=types::Id")), "ClOrdId check");
  BOOST_CHECK_MESSAGE(((std::string)d.entries["Side"] == std::string("field=Side, offset=16, type=types::Side")), "Side check");
  BOOST_CHECK_MESSAGE(((std::string)d.entries["TransactionTime"] == std::string("field=TransactionTime, offset=20, type=types::Datetime")), "TransactionTime check");
  BOOST_CHECK_MESSAGE(((std::string)d.entries["Underlyings"] == std::string("field=Underlyings, offset=26, type=smunix::off::Ref<smunix::dyn::arr<onixs::Instrument, smunix::check::NotBound, unsigned short> >")), "Underlyings check");
  BOOST_CHECK_MESSAGE(((std::string)d.entries["InstrumentLegs"] == std::string("field=InstrumentLegs, offset=28, type=smunix::off::Ref<smunix::dyn::arr<onixs::InstrumentLegBlock, smunix::check::NotBound, unsigned short> >")), "InstrumentLegs check");
}

namespace smunix {
  namespace str {
    template<> struct Conv<types::Type> {
      static std::string apply(r<types::Type> t) {
        return "AB";
      }
    };
  
    template<> struct Conv<types::Id> {
      static std::string apply(r<types::Id> t) {
        return to(t.value());
      }
    };

    template<> struct Conv<types::Qty> {
      static std::string apply(r<types::Qty> t) {
        return to(t.value());
      }
    };
  
    template<> struct Conv<types::Side> {
      static std::string apply(r<types::Side> t) {
        switch(t) {
        case types::Side::Buy : return "Buy";
        case types::Side::Sell : return "Sell";
        case types::Side::BuyMinus : return "BuyMinus";
        case types::Side::SellPlus : return "SellPlus";
        case types::Side::SellShort : return "SellShort";
        default: return "unknown";
        }
        return "unknown";
      }
    };

    template<> struct Conv<types::SwapType> {
      static std::string apply(r<types::SwapType> t) {
        switch(t) {
        case types::SwapType::Bear : return "Bear";
        default: return "unknown";
        }
        return "unknown";
      }
    };

    template<> struct Conv<types::Date> {
      static std::string apply(r<types::Date> t) {
        return to(t.yy())
          + ":" + to(t.mm())
          + ":" + to(t.dd())
          ;
      }
    };
  
    template<> struct Conv<types::Time> {
      static std::string apply(r<types::Time> t) {
        return to(t.hh())
          + ":" + to(t.mm())
          + ":" + to(t.ss())
          ;
      }
    };
  
    template<> struct Conv<types::Datetime> {
      static std::string apply(r<types::Datetime> t) {
        return conv(t.date()) + "_" + conv(t.time());
      }
    };
  } // namespace str  
} // namespace smunix

struct NewOrderMultilegToFIX {
  template<class T> void applyTag(uint16_t n, r<T> aField) {
    tag::fix::Conv<T>::apply(os, n, aField, *this);
  }
  template<class T> void applyGrp(uint16_t n, r<T> aField) {
    grp::fix::Conv<T>::apply(os, n, aField, *this);
  }
  operator std::string () const {
    return (*os).str();
  }
  stream::Out<std::ostringstream> os;
};

template<class Msg> struct NewOrderMultileg_To_FIX_SizeCheck;
// Notes : below we can see that the size of aligned structured data is way too big compared to simple FIX. It ends up that network transmission will be slower, but processing structured data will end up being faster.
// This calls for a definition of a new networking formatting.
template<class T, uint16_t C> struct NewOrderMultileg_To_FIX_SizeCheck<buffer::Message<T, C, buffer::AlignType::WITH_ALIGN>> {
  static constexpr size_t sizeof_fixmsg() { return 144; }
  static constexpr size_t strlen_fixmsg() { return 143; }
  static constexpr size_t buffer_fixmsg() { return 171; }
};

template<class T, uint16_t C> struct NewOrderMultileg_To_FIX_SizeCheck<buffer::Message<T, C, buffer::AlignType::WITHOUT_ALIGN>> {
  static constexpr size_t sizeof_fixmsg() { return 144; }
  static constexpr size_t strlen_fixmsg() { return 143; }
  static constexpr size_t buffer_fixmsg() { return 160; }
};

template<class Msg> void NewOrderMultileg_To_FIX(r<bool> success, r<onixs::NewOrderMultileg> noml, Msg&& msg) {
  NewOrderMultilegToFIX f;
  noml.get(success, f);
  char fixmsg[] = "35=AB;11=3735928559;54=Buy;60=15:11:25_7:9:52;711=2;55=IBM;65=IBM.n;55=GOOGL;65=GOOGL.n;555=2;687=20;690=Bear;624=Buy;687=30;690=Bear;624=Sell;";
  BOOST_CHECK_MESSAGE((std::string)f == fixmsg, "NewOrderMultileg -> FIX conversion");
  BOOST_CHECK_MESSAGE(success, "Convert NewOrderMultileg -> FIX conversion");
  BOOST_TEST_MESSAGE((std::string)f);
  BOOST_CHECK_EQUAL(sizeof(fixmsg), NewOrderMultileg_To_FIX_SizeCheck<typename std::decay<Msg>::type>::sizeof_fixmsg());
  BOOST_CHECK_EQUAL(std::strlen(fixmsg), NewOrderMultileg_To_FIX_SizeCheck<typename std::decay<Msg>::type>::strlen_fixmsg());
  BOOST_CHECK_EQUAL(msg.header.next, NewOrderMultileg_To_FIX_SizeCheck<typename std::decay<Msg>::type>::buffer_fixmsg());
}

BOOST_AUTO_TEST_CASE(test_describe_NewOrderMultileg)
{
  using Msg = buffer::Message<uint8_t, MESSAGESZ>;
  Msg msg;
  bool success = false;
  r<onixs::NewOrderMultileg> noml = msg.alloc<onixs::NewOrderMultileg>(success);
  init_NewOrderMultileg(success, noml, msg);
  describe_NewOrderMultileg(success, noml, msg);
}

BOOST_AUTO_TEST_CASE(test_NewOrderMultileg_To_FIX)
{
  { // without alignment in the buffer
    using Msg = buffer::Message<uint8_t, MESSAGESZ, buffer::AlignType::WITHOUT_ALIGN>;
    Msg msg;
    bool success = false;
    r<onixs::NewOrderMultileg> noml = msg.alloc<onixs::NewOrderMultileg>(success);
    init_NewOrderMultileg(success, noml, msg);
    NewOrderMultileg_To_FIX(success, noml, msg);
  }
  { // with alignment in the buffer
    using Msg = buffer::Message<uint8_t, MESSAGESZ, buffer::AlignType::WITH_ALIGN>;
    Msg msg;
    bool success = false;
    r<onixs::NewOrderMultileg> noml = msg.alloc<onixs::NewOrderMultileg>(success);
    init_NewOrderMultileg(success, noml, msg);
    NewOrderMultileg_To_FIX(success, noml, msg);
  }
}

BOOST_AUTO_TEST_CASE(test_serialize_deserialize_NewOrderMultileg)
{
  {
    using Msg = buffer::Message<uint8_t, MESSAGESZ, buffer::AlignType::WITH_ALIGN>;
    static constexpr size_t countN = 10;
    std::vector<Msg> msgs;
    for (size_t i = 0; i < countN; ++i)
      msgs.emplace_back();
    bool success = false;
    r<onixs::NewOrderMultileg> noml1 = msgs[0].alloc<onixs::NewOrderMultileg>(success);
    init_NewOrderMultileg(success, noml1, msgs[0]);
    for (size_t i = 1; i < countN; ++i)
      std::memcpy(addr(msgs[i]), addr(msgs[0]), sizeof(msgs[0]));
    for(auto& msg: msgs) {
      auto& noml = msg.cast<onixs::NewOrderMultileg>(success);
      BOOST_CHECK_MESSAGE(success, "Cast MultiLeg order check");
      check_init_NewOrderMultileg(success, noml, msg);
    }
  }
  {
    using Msg = buffer::Message<uint8_t, MESSAGESZ, buffer::AlignType::WITHOUT_ALIGN>;
    static constexpr size_t countN = 10;
    std::vector<Msg> msgs;
    for (size_t i = 0; i < countN; ++i)
      msgs.emplace_back();
    bool success = false;
    r<onixs::NewOrderMultileg> noml1 = msgs[0].alloc<onixs::NewOrderMultileg>(success);
    init_NewOrderMultileg(success, noml1, msgs[0]);
    for (size_t i = 1; i < countN; ++i)
      std::memcpy(addr(msgs[i]), addr(msgs[0]), sizeof(msgs[0]));
    for(auto& msg: msgs) {
      auto& noml = msg.cast<onixs::NewOrderMultileg>(success);
      BOOST_CHECK_MESSAGE(success, "Cast MultiLeg order check");
      check_init_NewOrderMultileg(success, noml, msg);
    }
  }
}
