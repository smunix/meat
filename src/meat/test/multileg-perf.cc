#include <boost/test/unit_test.hpp>
#include <smunix/core-module.hh>
#include "onixs.biz/cpp/multi-leg/ab.h"

using namespace smunix;
static constexpr size_t CACHELINE = (1 << 6);
static constexpr size_t MESSAGESZ = 3*CACHELINE; // 3x cache line size

namespace onixs {
  template<class Msg> void Encode_NewOrderMultileg(r<bool> success, r<NewOrderMultileg> noml, Msg&& msg) {
    msg.allocRef(success, noml.Underlyings(), 2);
    {
      auto& underlying = (*noml.Underlyings()).at(0);
      msg.allocRef(success, underlying.Symbol(), sizeof("IBM")-1); // IBM
      *underlying.Symbol() = "IBM";
      msg.allocRef(success, underlying.SymbolSfx(), sizeof("IBM.n")-1); // IBM.n
      *underlying.SymbolSfx() = "IBM.n";
    }
    {
      auto& underlying = (*noml.Underlyings()).at(1);
      msg.allocRef(success, underlying.Symbol(), sizeof("GOOGL")-1); // GOOGL
      *underlying.Symbol() = "GOOGL";
      msg.allocRef(success, underlying.SymbolSfx(), sizeof("GOOGL.n")-1); // GOOGL.n
      *underlying.SymbolSfx() = "GOOGL.n";
    }
    msg.allocRef(success, noml.InstrumentLegs(), 2);
    {
      auto& instrmt = (*noml.InstrumentLegs()).at(0);
      msg.allocRef(success, instrmt.Leg().LegMaturityData());
      auto& leg = instrmt.Leg();
      msg.allocRef(success, instrmt.Leg().LegSymbol(), sizeof("IBM-STK-0")-1); // IBM
      *leg.LegSymbol() = "IBM-STK-0";
      auto& legMaturity = *leg.LegMaturityData();
      legMaturity.yy() = 15;
      legMaturity.mm() = 11;
      legMaturity.dd() = 24;
      instrmt.LegQty() = types::Qty {{20}};
      instrmt.LegSide() = types::Side::Buy;
      instrmt.LegSwapType() = types::SwapType::Bear;
    }
    {
      auto& instrmt = (*noml.InstrumentLegs()).at(1);
      msg.allocRef(success, instrmt.Leg().LegMaturityData());
      auto& leg = instrmt.Leg();
      msg.allocRef(success, instrmt.Leg().LegSymbol(), sizeof("GOOGL-STK-1")-1); // GOOGL
      *leg.LegSymbol() = "GOOGL-STK-1";
      auto& legMaturity = *leg.LegMaturityData();
      legMaturity.yy() = 15;
      legMaturity.mm() = 11;
      legMaturity.dd() = 30;
      instrmt.LegQty() = types::Qty {{30}} ;
      instrmt.LegSide() = types::Side::Sell;
      instrmt.LegSwapType() = types::SwapType::Bear;
    }
    noml.MsgType() = types::Type::AB;
    noml.ClOrdId().value() = 0xdeadbeef;
    noml.Side() = types::Side::Buy;
  
    auto& rdate = noml.TransactionTime().date();
    rdate.yy() = 15;
    rdate.mm() = 11;
    rdate.dd() = 25;
  
    auto& rtime = noml.TransactionTime().time();
    rtime.hh() = 7;
    rtime.mm() = 9;
    rtime.ss() = 52;  
  }
} // namespace onixs

namespace raw {
  struct InstrumentLeg {
    data<fix::arr<char, 5>> _LegSymbol;
    data<fix::arr<char, 5>> _LegSymbolSfx;
    data<types::Date> _LegMaturityData;

    r<fix::arr<char, 5>> LegSymbol() { return *_LegSymbol; }
    r<fix::arr<char, 5>> LegSymbolSfx() { return *_LegSymbolSfx; }
    r<types::Date> LegMaturityData() { return *_LegMaturityData; }

    r<fix::arr<char, 5> const> LegSymbol() const { return *_LegSymbol; }
    r<fix::arr<char, 5> const> LegSymbolSfx() const { return *_LegSymbolSfx; }
    r<types::Date const> LegMaturityData() const { return *_LegMaturityData; }
  };
  struct Instrument {
    data<fix::arr<char, 5>> _Symbol;
    data<fix::arr<char, 5>> _SymbolSfx;
    data<types::Date> _MaturityData;

    r<fix::arr<char, 5> const> Symbol () const { return *_Symbol; }
    r<fix::arr<char, 5> const> SymbolSfx () const { return *_SymbolSfx; }
    r<types::Date const> MaturityData () const { return *_MaturityData; }

    r<fix::arr<char, 5>> Symbol () { return *_Symbol; }
    r<fix::arr<char, 5>> SymbolSfx () { return *_SymbolSfx; }
    r<types::Date> MaturityData () { return *_MaturityData; }
  };
  struct InstrumentLegBlock {
    data<InstrumentLeg> _Leg;
    data<types::Qty> _LegQty;
    data<types::SwapType> _LegSwapType;
    data<types::Side> _LegSide;

    r<InstrumentLeg> Leg() { return *_Leg; }
    r<types::Qty> LegQty() { return *_LegQty; }
    r<types::SwapType> LegSwapType() { return *_LegSwapType; }
    r<types::Side> LegSide() { return *_LegSide; }

    r<InstrumentLeg const> Leg() const { return *_Leg; }
    r<types::Qty const> LegQty() const { return *_LegQty; }
    r<types::SwapType const> LegSwapType() const { return *_LegSwapType; }
    r<types::Side const> LegSide() const { return *_LegSide; }

  };
  struct NewOrderMultileg {
    data<types::Type> _MsgType;
    data<types::Id> _ClOrdId;
    data<types::Side> _Side;
    data<types::Datetime> _TransactionTime;
    data<fix::arr<Instrument, 2>> _Underlyings;
    data<fix::arr<InstrumentLegBlock, 2>> _InstrumentLegs;

    r<types::Type> MsgType() { return *_MsgType; }
    r<types::Id> ClOrdId() { return *_ClOrdId; }
    r<types::Side> Side() { return *_Side; }
    r<types::Datetime> TransactionTime() { return *_TransactionTime; }
    r<fix::arr<Instrument, 2>> Underlyings() { return *_Underlyings; }
    r<fix::arr<InstrumentLegBlock, 2>> InstrumentLegs() { return *_InstrumentLegs; }

    r<types::Type const> MsgType() const { return *_MsgType; }
    r<types::Id const> ClOrdId() const { return *_ClOrdId; }
    r<types::Side const> Side() const { return *_Side; }
    r<types::Datetime const> TransactionTime() const { return *_TransactionTime; }
    r<fix::arr<Instrument, 2> const> Underlyings() const { return *_Underlyings; }
    r<fix::arr<InstrumentLegBlock, 2> const> InstrumentLegs() const { return *_InstrumentLegs; }

  };

  template<class Msg> void Encode_NewOrderMultileg(r<bool> success, r<NewOrderMultileg> noml, Msg&& msg) {
    {
      auto& underlying = noml.Underlyings().at(0);
      underlying.Symbol() = "AAAAAAAAAAAA";
      underlying.SymbolSfx() = "AAAAAAAAAAAAA";
    }
    {
      auto& underlying = noml.Underlyings().at(1);
      underlying.Symbol() = "AAAAAAAAAAAA";
      underlying.SymbolSfx() = "AAAAAAAAAAAAA";
    }
    {
      auto& instrmt = noml.InstrumentLegs().at(0);
      auto& leg = instrmt.Leg();
      leg.LegSymbol() = "AAAAAAAAAAAAAA";
      auto& legMaturity = leg.LegMaturityData();
      legMaturity.yy() = 15;
      legMaturity.mm() = 11;
      legMaturity.dd() = 24;
      instrmt.LegQty() = types::Qty {{20}};
      instrmt.LegSide() = types::Side::Buy;
      instrmt.LegSwapType() = types::SwapType::Bear;
    }
    {
      auto& instrmt = noml.InstrumentLegs().at(1);
      auto& leg = instrmt.Leg();
      leg.LegSymbol() = "AAAAAAAAAAAAAA";
      auto& legMaturity = leg.LegMaturityData();
      legMaturity.yy() = 15;
      legMaturity.mm() = 11;
      legMaturity.dd() = 30;
      instrmt.LegQty() = types::Qty {{30}};
      instrmt.LegSide() = types::Side::Sell;
      instrmt.LegSwapType() = types::SwapType::Bear;
    }
    noml.MsgType() = types::Type::AB;
    noml.ClOrdId().value() = 0xdeadbeef;
    noml.Side() = types::Side::Buy;
  
    auto& rdate = noml.TransactionTime().date();
    rdate.yy() = 15;
    rdate.mm() = 11;
    rdate.dd() = 25;
  
    auto& rtime = noml.TransactionTime().time();
    rtime.hh() = 7;
    rtime.mm() = 9;
    rtime.ss() = 52;  
  }
} // namespace raw

namespace ronixs {
  template<class Msg> void Encode_NewOrderMultileg(r<bool> success, r<NewOrderMultileg> noml, Msg&& msg) {
    {
      auto& underlying = noml.Underlyings().at(0);
      underlying.Symbol() = "AAAAAAAAAAAA";
      underlying.SymbolSfx() = "AAAAAAAAAAAAA";
    }
    {
      auto& underlying = noml.Underlyings().at(1);
      underlying.Symbol() = "AAAAAAAAAAAA";
      underlying.SymbolSfx() = "AAAAAAAAAAAAA";
    }
    {
      auto& instrmt = noml.InstrumentLegs().at(0);
      auto& leg = instrmt.Leg();
      leg.LegSymbol() = "AAAAAAAAAAAAAA";
      auto& legMaturity = leg.LegMaturityData();
      legMaturity.yy() = 15;
      legMaturity.mm() = 11;
      legMaturity.dd() = 24;
      instrmt.LegQty() = types::Qty {{20}};
      instrmt.LegSide() = types::Side::Buy;
      instrmt.LegSwapType() = types::SwapType::Bear;
    }
    {
      auto& instrmt = noml.InstrumentLegs().at(1);
      auto& leg = instrmt.Leg();
      leg.LegSymbol() = "AAAAAAAAAAAAAA";
      auto& legMaturity = leg.LegMaturityData();
      legMaturity.yy() = 15;
      legMaturity.mm() = 11;
      legMaturity.dd() = 30;
      instrmt.LegQty() = types::Qty {{30}};
      instrmt.LegSide() = types::Side::Sell;
      instrmt.LegSwapType() = types::SwapType::Bear;
    }
    noml.MsgType() = types::Type::AB;
    noml.ClOrdId().value() = 0xdeadbeef;
    noml.Side() = types::Side::Buy;
  
    auto& rdate = noml.TransactionTime().date();
    rdate.yy() = 15;
    rdate.mm() = 11;
    rdate.dd() = 25;
  
    auto& rtime = noml.TransactionTime().time();
    rtime.hh() = 7;
    rtime.mm() = 9;
    rtime.ss() = 52;  
  }
} // namespace ronixs

struct TimedEvent {
  using clock_t = std::chrono::high_resolution_clock;
  using time_point_t = double;
  using interval_t = double;
  interval_t asInterval() const {
    return end - begin;
  }
  static std::string toString(interval_t i) {
    return str::to(i) + " ns";
  }
  time_point_t begin;
  time_point_t end;
};

template<size_t M, class NOML, buffer::AlignType ALGN, void(*Fn)(r<bool>, r<NOML>, r<buffer::Message<uint8_t, MESSAGESZ, ALGN>>)> struct Param {
  using Msg = buffer::Message<uint8_t, MESSAGESZ, ALGN>;
  static constexpr size_t MAX_STATS = M;
  using Stat = stats::Base<TimedEvent, MAX_STATS>;
  static void apply() {
    Msg msg;
    bool success = false;
    auto invoke = [&msg,&success]() {
      msg.reset();
      r<NOML> noml = msg.template alloc<NOML>(success);
      Fn(success, noml, msg);
    };
    auto epoch = TimedEvent::clock_t::now();
    auto startF = [&epoch](r<TimedEvent> ev){ ev.begin = std::chrono::duration_cast<std::chrono::nanoseconds>(TimedEvent::clock_t::now() - epoch).count(); };
    auto endF = [&epoch](r<TimedEvent> ev){ ev.end = std::chrono::duration_cast<std::chrono::nanoseconds>(TimedEvent::clock_t::now() - epoch).count(); };
    Stat stat;
    // warming up
    stream::Out<std::ostringstream> osw, osi;
    stat.warmup(osw, startF, endF, stats::Report::Simple, MAX_STATS / 100);
    stat.run(startF, invoke, endF);
    stat.show(osi, cpp::demangle<Param>(), stats::Report::NoMaxPercent);
    BOOST_TEST_MESSAGE((*osi).str());
  }
};
template<size_t M> struct Perf_Encode_NewOrderMultileg {
  static void apply() {
    Param<M, raw::NewOrderMultileg,	buffer::AlignType::WITH_ALIGN,		&raw::Encode_NewOrderMultileg>::apply();
    Param<M, ronixs::NewOrderMultileg,	buffer::AlignType::WITH_ALIGN,		&ronixs::Encode_NewOrderMultileg>::apply();
    Param<M, onixs::NewOrderMultileg,	buffer::AlignType::WITH_ALIGN,		&onixs::Encode_NewOrderMultileg>::apply();
    Param<M, onixs::NewOrderMultileg,	buffer::AlignType::WITHOUT_ALIGN,	&onixs::Encode_NewOrderMultileg>::apply();
  }
};

BOOST_AUTO_TEST_CASE(test_Perf_Encode_NewOrderMultileg)
{
  linear::Regression<Perf_Encode_NewOrderMultileg, 1000, 100000, 1000000>::apply();
}
