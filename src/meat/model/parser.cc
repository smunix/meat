// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.

#line 37 "src/meat/model/parser.cc" // lalr1.cc:404

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include <smunix/meat/parser.hh>

// User implementation prologue.

#line 51 "src/meat/model/parser.cc" // lalr1.cc:412
// Unqualified %code blocks.
#line 35 "pgen/parser.y" // lalr1.cc:413

#include <smunix/meat/driver.hh>
extern bool shouldIndent();
extern void shouldIndent(bool);

#line 59 "src/meat/model/parser.cc" // lalr1.cc:413


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

#line 13 "pgen/parser.y" // lalr1.cc:479
namespace  meat { namespace yy  {
#line 145 "src/meat/model/parser.cc" // lalr1.cc:479

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
   parser ::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              // Fall through.
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }


  /// Build a parser object.
   parser :: parser  (meat::driver& drv_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      drv (drv_yyarg)
  {}

   parser ::~ parser  ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
   parser ::by_state::by_state ()
    : state (empty_state)
  {}

  inline
   parser ::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
   parser ::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
   parser ::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
   parser ::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
   parser ::symbol_number_type
   parser ::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
   parser ::stack_symbol_type::stack_symbol_type ()
  {}


  inline
   parser ::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 38: // "boolv"
        value.move< bool > (that.value);
        break;

      case 39: // "charv"
        value.move< char > (that.value);
        break;

      case 41: // "doublev"
        value.move< double > (that.value);
        break;

      case 40: // "floatv"
        value.move< float > (that.value);
        break;

      case 35: // "int16_tv"
        value.move< int16_t > (that.value);
        break;

      case 36: // "int32_tv"
        value.move< int32_t > (that.value);
        break;

      case 37: // "int64_tv"
        value.move< int64_t > (that.value);
        break;

      case 34: // "int8_tv"
        value.move< int8_t > (that.value);
        break;

      case 67: // EDefinition
        value.move< meat::Definition::sp > (that.value);
        break;

      case 70: // EEnumItem
        value.move< meat::Enum::Entry::sp > (that.value);
        break;

      case 68: // EEnumeration
        value.move< meat::Enum::sp > (that.value);
        break;

      case 75: // EExtendField
      case 79: // EField
        value.move< meat::Field::sp > (that.value);
        break;

      case 59: // afile
        value.move< meat::File::sp > (that.value);
        break;

      case 62: // EImport
        value.move< meat::Import::sp > (that.value);
        break;

      case 65: // EModule
        value.move< meat::Module::sp > (that.value);
        break;

      case 71: // ERecord
        value.move< meat::Record::sp > (that.value);
        break;

      case 82: // EArithType
      case 83: // ESufType
      case 84: // EScalarType
        value.move< meat::ScalarType::sp > (that.value);
        break;

      case 80: // ETypeDef
        value.move< meat::TypeDefinition::sp > (that.value);
        break;

      case 81: // ETypePref
        value.move< meat::TypePrefix::sp > (that.value);
        break;

      case 85: // ETypeSufQual
        value.move< meat::TypeSfxQual::sp > (that.value);
        break;

      case 66: // EDefinitions
        value.move< smunix::sp<meat::Definition::Map> > (that.value);
        break;

      case 69: // EEnumItems
        value.move< smunix::sp<meat::Enum::Entry::Map> > (that.value);
        break;

      case 72: // EExtendBlock
      case 73: // EExtendBlockTail
      case 74: // EExtendFields
      case 77: // EFieldsBlock
      case 78: // EFields
        value.move< smunix::sp<meat::Field::Map> > (that.value);
        break;

      case 60: // EImportsBlock
      case 61: // EImports
        value.move< smunix::sp<meat::Import::Set> > (that.value);
        break;

      case 63: // EModulesBlock
        value.move< smunix::sp<meat::Module::Map> > (that.value);
        break;

      case 28: // "id"
      case 29: // "stringv"
      case 76: // EScopedIds
      case 86: // ELabel
        value.move< std::string > (that.value);
        break;

      case 31: // "uint16_tv"
      case 42: // "fixtagv"
        value.move< uint16_t > (that.value);
        break;

      case 32: // "uint32_tv"
        value.move< uint32_t > (that.value);
        break;

      case 33: // "uint64_tv"
        value.move< uint64_t > (that.value);
        break;

      case 30: // "uint8_tv"
        value.move< uint8_t > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
   parser ::stack_symbol_type&
   parser ::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 38: // "boolv"
        value.copy< bool > (that.value);
        break;

      case 39: // "charv"
        value.copy< char > (that.value);
        break;

      case 41: // "doublev"
        value.copy< double > (that.value);
        break;

      case 40: // "floatv"
        value.copy< float > (that.value);
        break;

      case 35: // "int16_tv"
        value.copy< int16_t > (that.value);
        break;

      case 36: // "int32_tv"
        value.copy< int32_t > (that.value);
        break;

      case 37: // "int64_tv"
        value.copy< int64_t > (that.value);
        break;

      case 34: // "int8_tv"
        value.copy< int8_t > (that.value);
        break;

      case 67: // EDefinition
        value.copy< meat::Definition::sp > (that.value);
        break;

      case 70: // EEnumItem
        value.copy< meat::Enum::Entry::sp > (that.value);
        break;

      case 68: // EEnumeration
        value.copy< meat::Enum::sp > (that.value);
        break;

      case 75: // EExtendField
      case 79: // EField
        value.copy< meat::Field::sp > (that.value);
        break;

      case 59: // afile
        value.copy< meat::File::sp > (that.value);
        break;

      case 62: // EImport
        value.copy< meat::Import::sp > (that.value);
        break;

      case 65: // EModule
        value.copy< meat::Module::sp > (that.value);
        break;

      case 71: // ERecord
        value.copy< meat::Record::sp > (that.value);
        break;

      case 82: // EArithType
      case 83: // ESufType
      case 84: // EScalarType
        value.copy< meat::ScalarType::sp > (that.value);
        break;

      case 80: // ETypeDef
        value.copy< meat::TypeDefinition::sp > (that.value);
        break;

      case 81: // ETypePref
        value.copy< meat::TypePrefix::sp > (that.value);
        break;

      case 85: // ETypeSufQual
        value.copy< meat::TypeSfxQual::sp > (that.value);
        break;

      case 66: // EDefinitions
        value.copy< smunix::sp<meat::Definition::Map> > (that.value);
        break;

      case 69: // EEnumItems
        value.copy< smunix::sp<meat::Enum::Entry::Map> > (that.value);
        break;

      case 72: // EExtendBlock
      case 73: // EExtendBlockTail
      case 74: // EExtendFields
      case 77: // EFieldsBlock
      case 78: // EFields
        value.copy< smunix::sp<meat::Field::Map> > (that.value);
        break;

      case 60: // EImportsBlock
      case 61: // EImports
        value.copy< smunix::sp<meat::Import::Set> > (that.value);
        break;

      case 63: // EModulesBlock
        value.copy< smunix::sp<meat::Module::Map> > (that.value);
        break;

      case 28: // "id"
      case 29: // "stringv"
      case 76: // EScopedIds
      case 86: // ELabel
        value.copy< std::string > (that.value);
        break;

      case 31: // "uint16_tv"
      case 42: // "fixtagv"
        value.copy< uint16_t > (that.value);
        break;

      case 32: // "uint32_tv"
        value.copy< uint32_t > (that.value);
        break;

      case 33: // "uint64_tv"
        value.copy< uint64_t > (that.value);
        break;

      case 30: // "uint8_tv"
        value.copy< uint8_t > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
   parser ::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
   parser ::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    switch (yytype)
    {
            case 28: // "id"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< std::string > (); }
#line 576 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 29: // "stringv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< std::string > (); }
#line 583 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 30: // "uint8_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< uint8_t > (); }
#line 590 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 31: // "uint16_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< uint16_t > (); }
#line 597 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 32: // "uint32_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< uint32_t > (); }
#line 604 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 33: // "uint64_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< uint64_t > (); }
#line 611 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 34: // "int8_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< int8_t > (); }
#line 618 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 35: // "int16_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< int16_t > (); }
#line 625 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 36: // "int32_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< int32_t > (); }
#line 632 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 37: // "int64_tv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< int64_t > (); }
#line 639 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 38: // "boolv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< bool > (); }
#line 646 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 39: // "charv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< char > (); }
#line 653 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 40: // "floatv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< float > (); }
#line 660 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 41: // "doublev"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< double > (); }
#line 667 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 42: // "fixtagv"

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< uint16_t > (); }
#line 674 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 59: // afile

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::File::sp > (); }
#line 681 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 60: // EImportsBlock

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Import::Set> > (); }
#line 688 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 61: // EImports

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Import::Set> > (); }
#line 695 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 62: // EImport

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Import::sp > (); }
#line 702 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 63: // EModulesBlock

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Module::Map> > (); }
#line 709 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 65: // EModule

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Module::sp > (); }
#line 716 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 66: // EDefinitions

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Definition::Map> > (); }
#line 723 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 67: // EDefinition

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Definition::sp > (); }
#line 730 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 68: // EEnumeration

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Enum::sp > (); }
#line 737 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 69: // EEnumItems

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Enum::Entry::Map> > (); }
#line 744 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 70: // EEnumItem

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Enum::Entry::sp > (); }
#line 751 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 71: // ERecord

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Record::sp > (); }
#line 758 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 72: // EExtendBlock

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Field::Map> > (); }
#line 765 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 73: // EExtendBlockTail

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Field::Map> > (); }
#line 772 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 74: // EExtendFields

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Field::Map> > (); }
#line 779 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 75: // EExtendField

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Field::sp > (); }
#line 786 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 76: // EScopedIds

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< std::string > (); }
#line 793 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 77: // EFieldsBlock

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Field::Map> > (); }
#line 800 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 78: // EFields

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< smunix::sp<meat::Field::Map> > (); }
#line 807 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 79: // EField

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::Field::sp > (); }
#line 814 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 80: // ETypeDef

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::TypeDefinition::sp > (); }
#line 821 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 81: // ETypePref

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::TypePrefix::sp > (); }
#line 828 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 82: // EArithType

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::ScalarType::sp > (); }
#line 835 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 83: // ESufType

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::ScalarType::sp > (); }
#line 842 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 84: // EScalarType

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::ScalarType::sp > (); }
#line 849 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 85: // ETypeSufQual

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< meat::TypeSfxQual::sp > (); }
#line 856 "src/meat/model/parser.cc" // lalr1.cc:636
        break;

      case 86: // ELabel

#line 130 "pgen/parser.y" // lalr1.cc:636
        { yyoutput << yysym.value.template as< std::string > (); }
#line 863 "src/meat/model/parser.cc" // lalr1.cc:636
        break;


      default:
        break;
    }
    yyo << ')';
  }
#endif

  inline
  void
   parser ::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
   parser ::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
   parser ::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
   parser ::debug_stream () const
  {
    return *yycdebug_;
  }

  void
   parser ::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


   parser ::debug_level_type
   parser ::debug_level () const
  {
    return yydebug_;
  }

  void
   parser ::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline  parser ::state_type
   parser ::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
   parser ::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
   parser ::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
   parser ::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    // User initialization code.
    #line 28 "pgen/parser.y" // lalr1.cc:745
{
    // initialize the initial location
    /* @$.begin.filename = @$.end.filename = drv.file(); */
    yyla.location.initialize(&drv.filenames.back());
}

#line 983 "src/meat/model/parser.cc" // lalr1.cc:745

    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex (drv));
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 38: // "boolv"
        yylhs.value.build< bool > ();
        break;

      case 39: // "charv"
        yylhs.value.build< char > ();
        break;

      case 41: // "doublev"
        yylhs.value.build< double > ();
        break;

      case 40: // "floatv"
        yylhs.value.build< float > ();
        break;

      case 35: // "int16_tv"
        yylhs.value.build< int16_t > ();
        break;

      case 36: // "int32_tv"
        yylhs.value.build< int32_t > ();
        break;

      case 37: // "int64_tv"
        yylhs.value.build< int64_t > ();
        break;

      case 34: // "int8_tv"
        yylhs.value.build< int8_t > ();
        break;

      case 67: // EDefinition
        yylhs.value.build< meat::Definition::sp > ();
        break;

      case 70: // EEnumItem
        yylhs.value.build< meat::Enum::Entry::sp > ();
        break;

      case 68: // EEnumeration
        yylhs.value.build< meat::Enum::sp > ();
        break;

      case 75: // EExtendField
      case 79: // EField
        yylhs.value.build< meat::Field::sp > ();
        break;

      case 59: // afile
        yylhs.value.build< meat::File::sp > ();
        break;

      case 62: // EImport
        yylhs.value.build< meat::Import::sp > ();
        break;

      case 65: // EModule
        yylhs.value.build< meat::Module::sp > ();
        break;

      case 71: // ERecord
        yylhs.value.build< meat::Record::sp > ();
        break;

      case 82: // EArithType
      case 83: // ESufType
      case 84: // EScalarType
        yylhs.value.build< meat::ScalarType::sp > ();
        break;

      case 80: // ETypeDef
        yylhs.value.build< meat::TypeDefinition::sp > ();
        break;

      case 81: // ETypePref
        yylhs.value.build< meat::TypePrefix::sp > ();
        break;

      case 85: // ETypeSufQual
        yylhs.value.build< meat::TypeSfxQual::sp > ();
        break;

      case 66: // EDefinitions
        yylhs.value.build< smunix::sp<meat::Definition::Map> > ();
        break;

      case 69: // EEnumItems
        yylhs.value.build< smunix::sp<meat::Enum::Entry::Map> > ();
        break;

      case 72: // EExtendBlock
      case 73: // EExtendBlockTail
      case 74: // EExtendFields
      case 77: // EFieldsBlock
      case 78: // EFields
        yylhs.value.build< smunix::sp<meat::Field::Map> > ();
        break;

      case 60: // EImportsBlock
      case 61: // EImports
        yylhs.value.build< smunix::sp<meat::Import::Set> > ();
        break;

      case 63: // EModulesBlock
        yylhs.value.build< smunix::sp<meat::Module::Map> > ();
        break;

      case 28: // "id"
      case 29: // "stringv"
      case 76: // EScopedIds
      case 86: // ELabel
        yylhs.value.build< std::string > ();
        break;

      case 31: // "uint16_tv"
      case 42: // "fixtagv"
        yylhs.value.build< uint16_t > ();
        break;

      case 32: // "uint32_tv"
        yylhs.value.build< uint32_t > ();
        break;

      case 33: // "uint64_tv"
        yylhs.value.build< uint64_t > ();
        break;

      case 30: // "uint8_tv"
        yylhs.value.build< uint8_t > ();
        break;

      default:
        break;
    }


      // Compute the default @$.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:
#line 134 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::File::sp > () = meat::alloc<meat::File>(yystack_[1].value.as< smunix::sp<meat::Import::Set> > (), yystack_[0].value.as< smunix::sp<meat::Module::Map> > ()); drv.set(yylhs.value.as< meat::File::sp > ()); }
#line 1225 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 3:
#line 137 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< smunix::sp<meat::Import::Set> > () = yystack_[1].value.as< smunix::sp<meat::Import::Set> > (); }
#line 1231 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 4:
#line 138 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Import::Set> > () = meat::alloc<meat::Import::Set>(); }
#line 1237 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 5:
#line 141 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Import::Set> > () = yystack_[1].value.as< smunix::sp<meat::Import::Set> > (); yylhs.value.as< smunix::sp<meat::Import::Set> > ()->emplace(yystack_[0].value.as< meat::Import::sp > ()); }
#line 1243 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 6:
#line 142 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Import::Set> > () = meat::alloc<meat::Import::Set>(); yylhs.value.as< smunix::sp<meat::Import::Set> > ()->emplace(yystack_[0].value.as< meat::Import::sp > ()); }
#line 1249 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 7:
#line 145 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Import::sp > () = meat::alloc<meat::Import>(yystack_[1].value.as< std::string > ()); }
#line 1255 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 8:
#line 148 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Module::Map> > () = yystack_[1].value.as< smunix::sp<meat::Module::Map> > (); smunix::asr(yylhs.value.as< smunix::sp<meat::Module::Map> > ())[yystack_[0].value.as< meat::Module::sp > ()->id()]=yystack_[0].value.as< meat::Module::sp > (); }
#line 1261 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 9:
#line 149 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Module::Map> > () = meat::alloc<meat::Module::Map>(); smunix::asr(yylhs.value.as< smunix::sp<meat::Module::Map> > ())[yystack_[0].value.as< meat::Module::sp > ()->id()] = yystack_[0].value.as< meat::Module::sp > (); }
#line 1267 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 12:
#line 156 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Module::sp > () = meat::alloc<meat::Module>(yystack_[5].value.as< std::string > (), yystack_[2].value.as< smunix::sp<meat::Definition::Map> > ()); }
#line 1273 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 13:
#line 157 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Module::sp > () = meat::alloc<meat::Module>(yystack_[6].value.as< std::string > (), yystack_[2].value.as< smunix::sp<meat::Definition::Map> > ()); }
#line 1279 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 14:
#line 158 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Module::sp > () = meat::alloc<meat::Module>(yystack_[4].value.as< std::string > (), yystack_[1].value.as< smunix::sp<meat::Definition::Map> > ()); }
#line 1285 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 15:
#line 159 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Module::sp > () = meat::alloc<meat::Module>(yystack_[5].value.as< std::string > (), yystack_[1].value.as< smunix::sp<meat::Definition::Map> > ()); }
#line 1291 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 16:
#line 162 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Definition::Map> > () = yystack_[1].value.as< smunix::sp<meat::Definition::Map> > (); smunix::asr(yylhs.value.as< smunix::sp<meat::Definition::Map> > ())[yystack_[0].value.as< meat::Definition::sp > ()->id()] = yystack_[0].value.as< meat::Definition::sp > (); }
#line 1297 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 17:
#line 163 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Definition::Map> > () = meat::alloc<meat::Definition::Map>(); smunix::asr(yylhs.value.as< smunix::sp<meat::Definition::Map> > ())[yystack_[0].value.as< meat::Definition::sp > ()->id()] = yystack_[0].value.as< meat::Definition::sp > (); }
#line 1303 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 18:
#line 166 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Definition::sp > () = yystack_[0].value.as< meat::Enum::sp > (); }
#line 1309 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 19:
#line 167 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Definition::sp > () = yystack_[0].value.as< meat::Record::sp > (); }
#line 1315 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 20:
#line 168 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Definition::sp > () = yystack_[0].value.as< meat::Module::sp > (); }
#line 1321 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 21:
#line 171 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Enum::sp > () = meat::alloc<meat::Enum>(yystack_[5].value.as< std::string > (), yystack_[2].value.as< smunix::sp<meat::Enum::Entry::Map> > ()); }
#line 1327 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 22:
#line 172 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Enum::sp > () = meat::alloc<meat::Enum>(yystack_[4].value.as< std::string > (), yystack_[2].value.as< smunix::sp<meat::Enum::Entry::Map> > ()); }
#line 1333 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 23:
#line 175 "pgen/parser.y" // lalr1.cc:859
    { smunix::asr(yystack_[1].value.as< smunix::sp<meat::Enum::Entry::Map> > ())[yystack_[0].value.as< meat::Enum::Entry::sp > ()->label] = yystack_[0].value.as< meat::Enum::Entry::sp > (); yylhs.value.as< smunix::sp<meat::Enum::Entry::Map> > () = yystack_[1].value.as< smunix::sp<meat::Enum::Entry::Map> > ();  }
#line 1339 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 24:
#line 176 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Enum::Entry::Map> > ()= meat::alloc<meat::Enum::Entry::Map>(); smunix::asr(yylhs.value.as< smunix::sp<meat::Enum::Entry::Map> > ())[yystack_[0].value.as< meat::Enum::Entry::sp > ()->label] = yystack_[0].value.as< meat::Enum::Entry::sp > (); }
#line 1345 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 25:
#line 179 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Enum::Entry::sp > () = meat::alloc<meat::Enum::Entry>(yystack_[2].value.as< std::string > (), yystack_[0].value.as< uint8_t > ()); }
#line 1351 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 26:
#line 180 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Enum::Entry::sp > () = meat::alloc<meat::Enum::Entry>(yystack_[2].value.as< std::string > (), yystack_[0].value.as< int64_t > ()); }
#line 1357 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 27:
#line 183 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Record::sp > () = meat::alloc<meat::Record>(yystack_[2].value.as< std::string > (), yystack_[0].value.as< smunix::sp<meat::Field::Map> > (), yystack_[1].value.as< smunix::sp<meat::Field::Map> > ()); }
#line 1363 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 28:
#line 184 "pgen/parser.y" // lalr1.cc:859
    { shouldIndent(false); yylhs.value.as< meat::Record::sp > () = meat::alloc<meat::Record>(yystack_[1].value.as< std::string > (), yystack_[0].value.as< smunix::sp<meat::Field::Map> > ()); }
#line 1369 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 29:
#line 187 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[0].value.as< smunix::sp<meat::Field::Map> > (); }
#line 1375 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 30:
#line 188 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[0].value.as< smunix::sp<meat::Field::Map> > (); }
#line 1381 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 31:
#line 191 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[1].value.as< smunix::sp<meat::Field::Map> > (); }
#line 1387 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 32:
#line 194 "pgen/parser.y" // lalr1.cc:859
    { smunix::asr(yystack_[2].value.as< smunix::sp<meat::Field::Map> > ())[yystack_[0].value.as< meat::Field::sp > ()->label] = yystack_[0].value.as< meat::Field::sp > (); yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[2].value.as< smunix::sp<meat::Field::Map> > (); }
#line 1393 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 33:
#line 195 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = meat::alloc<meat::Field::Map>(); smunix::asr(yylhs.value.as< smunix::sp<meat::Field::Map> > ())[yystack_[0].value.as< meat::Field::sp > ()->label] = yystack_[0].value.as< meat::Field::sp > (); }
#line 1399 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 34:
#line 198 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Field::sp > () = meat::alloc<meat::Field>(yystack_[0].value.as< std::string > (), meat::alloc<meat::TypeDefinition>(meat::alloc<ScalarTypePrefix>(meat::alloc<meat::ScalarType>(yystack_[2].value.as< std::string > ())))); }
#line 1405 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 35:
#line 199 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Field::sp > () = meat::alloc<meat::Field>(yystack_[0].value.as< std::string > (), meat::alloc<meat::TypeDefinition>(meat::alloc<ScalarTypePrefix>(meat::alloc<meat::ScalarType>(yystack_[2].value.as< std::string > ())))); }
#line 1411 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 36:
#line 202 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< std::string > () = yystack_[2].value.as< std::string > () + "." + yystack_[0].value.as< std::string > (); }
#line 1417 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 37:
#line 203 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< std::string > () = yystack_[0].value.as< std::string > (); }
#line 1423 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 38:
#line 206 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[2].value.as< smunix::sp<meat::Field::Map> > (); }
#line 1429 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 39:
#line 207 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[2].value.as< smunix::sp<meat::Field::Map> > (); }
#line 1435 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 40:
#line 208 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[1].value.as< smunix::sp<meat::Field::Map> > (); }
#line 1441 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 41:
#line 211 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = yystack_[1].value.as< smunix::sp<meat::Field::Map> > (); smunix::asr(yylhs.value.as< smunix::sp<meat::Field::Map> > ())[yystack_[0].value.as< meat::Field::sp > ()->label] = yystack_[0].value.as< meat::Field::sp > (); }
#line 1447 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 42:
#line 212 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< smunix::sp<meat::Field::Map> > () = meat::alloc<meat::Field::Map>(); smunix::asr(yylhs.value.as< smunix::sp<meat::Field::Map> > ())[yystack_[0].value.as< meat::Field::sp > ()->label] = yystack_[0].value.as< meat::Field::sp > (); }
#line 1453 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 43:
#line 215 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::Field::sp > () = meat::alloc<meat::Field>(yystack_[3].value.as< std::string > (), yystack_[1].value.as< meat::TypeDefinition::sp > ()); }
#line 1459 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 44:
#line 217 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypeDefinition::sp > () = meat::alloc<TypeDefinition>(yystack_[1].value.as< meat::TypePrefix::sp > (), yystack_[0].value.as< meat::TypeSfxQual::sp > ());  }
#line 1465 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 45:
#line 220 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypePrefix::sp > () = meat::alloc<meat::ScalarTypePrefix>(yystack_[0].value.as< meat::ScalarType::sp > ()); }
#line 1471 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 46:
#line 221 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypePrefix::sp > () = meat::alloc<meat::ReferencedTypePrefix>(meat::alloc<meat::DynArrTypePrefix>(yystack_[1].value.as< meat::TypePrefix::sp > ())); }
#line 1477 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 47:
#line 222 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypePrefix::sp > () = meat::alloc<meat::FixArrTypePrefix>(yystack_[3].value.as< meat::TypePrefix::sp > (), yystack_[1].value.as< uint8_t > ()); }
#line 1483 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 48:
#line 223 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypePrefix::sp > () = meat::alloc<meat::FixArrTypePrefix>(yystack_[3].value.as< meat::TypePrefix::sp > (), yystack_[1].value.as< int64_t > ()); }
#line 1489 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 49:
#line 224 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypePrefix::sp > () = meat::alloc<meat::OptionalTypePrefix>(yystack_[1].value.as< meat::TypePrefix::sp > ()); }
#line 1495 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 50:
#line 225 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypePrefix::sp > () = meat::alloc<meat::ReferencedTypePrefix>(yystack_[0].value.as< meat::TypePrefix::sp > ()); }
#line 1501 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 51:
#line 228 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("uint8_t"); }
#line 1507 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 52:
#line 229 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("uint16_t"); }
#line 1513 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 53:
#line 230 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("uint32_t"); }
#line 1519 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 54:
#line 231 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("uint64_t"); }
#line 1525 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 55:
#line 232 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("int8_t"); }
#line 1531 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 56:
#line 233 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("int16_t"); }
#line 1537 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 57:
#line 234 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("int32_t"); }
#line 1543 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 58:
#line 235 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("int64_t"); }
#line 1549 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 59:
#line 236 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("float"); }
#line 1555 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 60:
#line 237 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("double"); }
#line 1561 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 61:
#line 240 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("string"); }
#line 1567 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 62:
#line 241 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("char"); }
#line 1573 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 63:
#line 242 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("bool"); }
#line 1579 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 64:
#line 243 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = yystack_[0].value.as< meat::ScalarType::sp > (); }
#line 1585 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 65:
#line 246 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("char"); }
#line 1591 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 66:
#line 247 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>("bool"); }
#line 1597 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 67:
#line 248 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = meat::alloc<meat::ScalarType>(yystack_[0].value.as< std::string > ()); }
#line 1603 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 68:
#line 249 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::ScalarType::sp > () = yystack_[0].value.as< meat::ScalarType::sp > (); }
#line 1609 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 69:
#line 252 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypeSfxQual::sp > () = meat::alloc<meat::TypeSfxQualTag>(yystack_[3].value.as< uint16_t > (), yystack_[1].value.as< meat::ScalarType::sp > ()); }
#line 1615 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 70:
#line 253 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypeSfxQual::sp > () = meat::alloc<meat::TypeSfxQualTag>(yystack_[3].value.as< int64_t > (), yystack_[1].value.as< meat::ScalarType::sp > ()); }
#line 1621 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 71:
#line 254 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypeSfxQual::sp > () = meat::alloc<meat::TypeSfxQualGrp>(yystack_[1].value.as< uint16_t > ()); }
#line 1627 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 72:
#line 255 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypeSfxQual::sp > () = meat::alloc<meat::TypeSfxQualGrp>(yystack_[1].value.as< int64_t > ()); }
#line 1633 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 73:
#line 256 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< meat::TypeSfxQual::sp > () = meat::alloc<meat::TypeSfxQual>(); }
#line 1639 "src/meat/model/parser.cc" // lalr1.cc:859
    break;

  case 74:
#line 259 "pgen/parser.y" // lalr1.cc:859
    { yylhs.value.as< std::string > () =  yystack_[0].value.as< std::string > (); }
#line 1645 "src/meat/model/parser.cc" // lalr1.cc:859
    break;


#line 1649 "src/meat/model/parser.cc" // lalr1.cc:859
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
   parser ::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
   parser ::yysyntax_error_ (state_type yystate, const symbol_type& yyla) const
  {
    // Number of reported tokens (one for the "unexpected", one per
    // "expected").
    size_t yycount = 0;
    // Its maximum.
    enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
    // Arguments of yyformat.
    char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];

    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yyla) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state
         merging (from LALR or IELR) and default reductions corrupt the
         expected token list.  However, the list is correct for
         canonical LR with one exception: it will still contain any
         token that will not be accepted due to an error action in a
         later state.
    */
    if (!yyla.empty ())
      {
        int yytoken = yyla.type_get ();
        yyarg[yycount++] = yytname_[yytoken];
        int yyn = yypact_[yystate];
        if (!yy_pact_value_is_default_ (yyn))
          {
            /* Start YYX at -YYN if negative to avoid negative indexes in
               YYCHECK.  In other words, skip the first -YYN actions for
               this state because they are default actions.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;
            // Stay within bounds of both yycheck and yytname.
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
              if (yycheck_[yyx + yyn] == yyx && yyx != yyterror_
                  && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
                {
                  if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                    {
                      yycount = 1;
                      break;
                    }
                  else
                    yyarg[yycount++] = yytname_[yyx];
                }
          }
      }

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
        YYCASE_(0, YY_("syntax error"));
        YYCASE_(1, YY_("syntax error, unexpected %s"));
        YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    std::string yyres;
    // Argument number.
    size_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += yytnamerr_ (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const signed char  parser ::yypact_ninf_ = -63;

  const signed char  parser ::yytable_ninf_ = -1;

  const short int
   parser ::yypact_[] =
  {
      22,    46,    93,   113,   104,   -63,    90,   113,   -63,   119,
      -7,   -63,   117,   -63,    96,   -63,   -63,    -3,    73,   -63,
     115,     9,   -63,    69,    72,    -6,   -63,   -63,   -63,   -63,
     115,    98,   101,   -63,   -63,     1,   -63,     3,    11,   -10,
     -63,   -63,     2,   118,    27,    86,   120,   122,    38,   -63,
     -63,    84,   123,   -63,   118,   120,   107,     5,   -63,    39,
     -63,   109,   -14,   -63,   125,   126,   -63,    88,   -16,   -63,
     -63,    -2,    56,   -63,    91,   -63,    42,   -15,    40,   -63,
     -63,     4,    28,   -63,   -13,   -63,    44,   -63,   -63,   -63,
     -63,   -63,   -63,   -63,   -63,   -63,   -63,   -63,   -63,   -63,
     -63,    40,    40,    40,    89,    94,    95,   -63,   -63,   -12,
     -63,    -2,   116,   121,   -63,   -63,    29,    92,   -63,   -63,
      58,   -63,   124,   -63,   -63,   -63,   -63,    59,   -63,    97,
     100,   -63,   108,   110,    -4,    60,   -63,   -63,   102,   105,
     106,   111,   -63,   -63,    87,    87,   -63,   -63,   -63,   -63,
     127,   128,   -63,   -63
  };

  const unsigned char
   parser ::yydefact_[] =
  {
       4,     0,     0,     0,     0,     1,     0,     2,     9,     0,
       0,     6,     0,     8,     0,     3,     5,     0,     0,    11,
       0,     0,     7,    11,     0,     0,    17,    18,    19,    10,
       0,     0,     0,    20,    14,     0,    16,     0,     0,     0,
      12,    15,     0,     0,     0,     0,     0,     0,     0,    28,
      13,     0,     0,    24,     0,     0,     0,     0,    42,     0,
      27,     0,     0,    23,     0,     0,    74,     0,     0,    40,
      41,     0,     0,    29,     0,    22,     0,     0,     0,    38,
      37,     0,     0,    33,     0,    30,     0,    21,    39,    51,
      52,    53,    54,    55,    56,    57,    58,    66,    65,    59,
      60,     0,     0,     0,    67,     0,    73,    68,    45,     0,
      31,     0,     0,     0,    25,    26,     0,     0,    50,    43,
       0,    44,     0,    32,    35,    36,    46,     0,    49,     0,
       0,    34,     0,     0,     0,     0,    47,    48,     0,     0,
       0,     0,    71,    72,     0,     0,    61,    63,    62,    64,
       0,     0,    69,    70
  };

  const short int
   parser ::yypgoto_[] =
  {
     -63,   -63,   -63,   -63,   135,   -63,   -17,   112,   129,     0,
     -63,   114,   -29,   -63,   -63,    75,   -63,    45,   -62,   130,
     131,    15,   -63,    19,   -46,    16,   -63,   -63,   -63
  };

  const short int
   parser ::yydefgoto_[] =
  {
      -1,     2,     3,    10,    11,     7,    24,    33,    25,    26,
      27,    52,    53,    28,    48,    73,    82,    83,   104,    49,
      57,    58,   105,   106,   107,   150,   108,   121,    67
  };

  const unsigned char
   parser ::yytable_[] =
  {
      21,   112,   122,    45,     6,     6,     9,    23,    35,    84,
      19,    19,    66,    66,    29,    29,    23,    29,    68,   109,
      42,    44,    29,    63,    19,    36,    80,   138,     1,    79,
      88,    75,    80,   139,    46,    63,    61,    36,    15,    34,
      29,    20,    72,    47,   113,   113,    40,    50,    41,    84,
      69,    45,    19,    30,    81,    43,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,    80,    29,
     129,    54,    70,   110,   114,     6,   126,    31,    32,   127,
      70,   115,    46,    71,   111,    29,   101,    87,   102,   132,
       4,   140,    61,     5,    81,   103,   133,   141,   149,   149,
      71,   130,   146,    89,    90,    91,    92,    93,    94,    95,
      96,   147,   148,    99,   100,     8,     6,     9,    12,    13,
     116,   117,   118,    14,    17,    18,    38,    22,    23,    39,
      55,    51,    59,    56,    61,    66,    62,    74,    76,    77,
      78,   128,    86,   120,   124,    16,   113,    85,   119,   125,
     134,   142,   131,   135,   143,   136,   123,   137,   144,    37,
       0,   151,     0,   145,     0,     0,     0,     0,    64,     0,
       0,     0,     0,     0,     0,     0,   152,   153,    60,     0,
       0,     0,     0,     0,     0,     0,    65
  };

  const short int
   parser ::yycheck_[] =
  {
      17,    14,    14,    13,     3,     3,    13,    13,    25,    71,
      13,    13,    28,    28,    13,    13,    13,    13,    13,    81,
      37,    38,    13,    52,    13,    25,    28,    31,     6,    45,
      45,    45,    28,    37,    44,    64,    50,    37,    45,    45,
      13,    44,    59,    53,    57,    57,    45,    45,    45,   111,
      45,    13,    13,    44,    71,    44,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    13,
      12,    44,    57,    45,    30,     3,    47,     8,     9,    50,
      65,    37,    44,    44,    56,    13,    46,    45,    48,    30,
      44,    31,    50,     0,   111,    55,    37,    37,   144,   145,
      44,    43,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,     3,     3,    13,    28,     7,
     101,   102,   103,     4,     7,    29,    28,    54,    13,    28,
      44,    13,    10,    13,    50,    28,    13,    28,    13,    13,
      52,    49,    51,    48,    28,    10,    57,    72,    54,    28,
      53,    49,    28,    53,    49,    47,   111,    47,    52,    30,
      -1,   145,    -1,    52,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    49,    49,    48,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    55
  };

  const unsigned char
   parser ::yystos_[] =
  {
       0,     6,    59,    60,    44,     0,     3,    63,    65,    13,
      61,    62,    28,    65,     4,    45,    62,     7,    29,    13,
      44,    64,    54,    13,    64,    66,    67,    68,    71,    13,
      44,     8,     9,    65,    45,    64,    67,    66,    28,    28,
      45,    45,    64,    44,    64,    13,    44,    53,    72,    77,
      45,    13,    69,    70,    44,    44,    13,    78,    79,    10,
      77,    50,    13,    70,    69,    78,    28,    86,    13,    45,
      79,    44,    64,    73,    28,    45,    13,    13,    52,    45,
      28,    64,    74,    75,    76,    73,    51,    45,    45,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    46,    48,    55,    76,    80,    81,    82,    84,    76,
      45,    56,    14,    57,    30,    37,    81,    81,    81,    54,
      48,    85,    14,    75,    28,    28,    47,    50,    49,    12,
      43,    28,    30,    37,    53,    53,    47,    47,    31,    37,
      31,    37,    49,    49,    52,    52,    15,    24,    25,    82,
      83,    83,    49,    49
  };

  const unsigned char
   parser ::yyr1_[] =
  {
       0,    58,    59,    60,    60,    61,    61,    62,    63,    63,
      64,    64,    65,    65,    65,    65,    66,    66,    67,    67,
      67,    68,    68,    69,    69,    70,    70,    71,    71,    72,
      72,    73,    74,    74,    75,    75,    76,    76,    77,    77,
      77,    78,    78,    79,    80,    81,    81,    81,    81,    81,
      81,    82,    82,    82,    82,    82,    82,    82,    82,    82,
      82,    83,    83,    83,    83,    84,    84,    84,    84,    85,
      85,    85,    85,    85,    86
  };

  const unsigned char
   parser ::yyr2_[] =
  {
       0,     2,     2,     4,     0,     2,     1,     4,     2,     1,
       2,     1,     7,     8,     6,     7,     2,     1,     1,     1,
       2,     8,     7,     2,     1,     5,     5,     5,     4,     3,
       4,     3,     3,     1,     4,     3,     3,     1,     4,     5,
       3,     2,     1,     5,     2,     1,     3,     5,     5,     3,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     7,
       7,     5,     5,     0,     1
  };



  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const  parser ::yytname_[] =
  {
  "\"end of file\"", "error", "$undefined", "\"module\"", "\"include\"",
  "\"import\"", "\"imports\"", "\"where\"", "\"enum\"", "\"record\"",
  "\"extend\"", "\"fix\"", "\"grp\"", "\"indent\"", "\"as\"", "\"string\"",
  "\"uint8_t\"", "\"uint16_t\"", "\"uint32_t\"", "\"uint64_t\"",
  "\"int8_t\"", "\"int16_t\"", "\"int32_t\"", "\"int64_t\"", "\"bool\"",
  "\"char\"", "\"float\"", "\"double\"", "\"id\"", "\"stringv\"",
  "\"uint8_tv\"", "\"uint16_tv\"", "\"uint32_tv\"", "\"uint64_tv\"",
  "\"int8_tv\"", "\"int16_tv\"", "\"int32_tv\"", "\"int64_tv\"",
  "\"boolv\"", "\"charv\"", "\"floatv\"", "\"doublev\"", "\"fixtagv\"",
  "\"tag\"", "\"{\"", "\"}\"", "\"[\"", "\"]\"", "\"(\"", "\")\"", "\"|\"",
  "\"=\"", "\"::\"", "\":\"", "\";\"", "\"@\"", "\",\"", "\".\"",
  "$accept", "afile", "EImportsBlock", "EImports", "EImport",
  "EModulesBlock", "EIndentsPlus", "EModule", "EDefinitions",
  "EDefinition", "EEnumeration", "EEnumItems", "EEnumItem", "ERecord",
  "EExtendBlock", "EExtendBlockTail", "EExtendFields", "EExtendField",
  "EScopedIds", "EFieldsBlock", "EFields", "EField", "ETypeDef",
  "ETypePref", "EArithType", "ESufType", "EScalarType", "ETypeSufQual",
  "ELabel", YY_NULLPTR
  };

#if YYDEBUG
  const unsigned short int
   parser ::yyrline_[] =
  {
       0,   134,   134,   137,   138,   141,   142,   145,   148,   149,
     152,   153,   156,   157,   158,   159,   162,   163,   166,   167,
     168,   171,   172,   175,   176,   179,   180,   183,   184,   187,
     188,   191,   194,   195,   198,   199,   202,   203,   206,   207,
     208,   211,   212,   215,   217,   220,   221,   222,   223,   224,
     225,   228,   229,   230,   231,   232,   233,   234,   235,   236,
     237,   240,   241,   242,   243,   246,   247,   248,   249,   252,
     253,   254,   255,   256,   259
  };

  // Print the state stack on the debug stream.
  void
   parser ::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
   parser ::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG


#line 13 "pgen/parser.y" // lalr1.cc:1167
} } //  meat::yy 
#line 2134 "src/meat/model/parser.cc" // lalr1.cc:1167
#line 262 "pgen/parser.y" // lalr1.cc:1168


namespace meat { namespace yy {
	void parser::error(parser::location_type const& loc, std::string const& msg) {
	    drv.error(loc, msg);
	}
    }
}
