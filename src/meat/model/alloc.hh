#pragma once
#include <smunix/core/memory.hh>

namespace meat {
  namespace s = smunix;
  template<class T, class ...Axs> s::sp<T> alloc(Axs&& ...axs) {
    return s::spAlloc<T>(std::forward<Axs>(axs)...);
  }
}
