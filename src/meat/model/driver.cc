#include <smunix/meat/driver.hh>

namespace meat {
  
  driver::driver() {}
  driver::~driver() {}
  
  int driver::parse(s::r<driver::filename const> f) {
    if (filters.insert(f).second) {
	filenames.emplace_back(f);
	begin();
	yy::parser p(*this);
	p.set_debug_level(trace_parsing);
	int res = p.parse();
	end();
	return res;
      }
    return -1; // bleh :-( !
  }
  void driver::error(yy::parser::location_type const& loc, std::string const& msg) {
  }
  void driver::error(std::string const& msg) {
  }
  void driver::trace_parse(bool v) {
    trace_parsing = v;
  }
  void driver::trace_scan(bool v) {
  }

} // namespace meat
